[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};
[[group(0), binding(0)]]
var<uniform> uniforms: Uniforms;

struct VertexInput {
	[[location(0)]] position: vec3<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] local_pos: vec3<f32>;
};

[[stage(vertex)]]
fn main(
	model: VertexInput
) -> VertexOutput {
	let rot_view = mat4x4<f32>(
		uniforms.view[0],
		uniforms.view[1],
		uniforms.view[2],
		vec4<f32>(0.0, 0.0, 0.0, 1.0)
	);
	var out: VertexOutput;
	out.local_pos = model.position;
	let clip_pos = uniforms.proj * rot_view * vec4<f32>(model.position, 1.0);
	out.clip_position = clip_pos.xyww;
	return out;
}

[[group(0), binding(1)]]
var t_cube: texture_cube<f32>;
[[group(0), binding(2)]]
var s_cube: sampler;

[[stage(fragment)]]
fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	let env_color = textureSample(t_cube, s_cube, in.local_pos).rgb;
	let env_color = env_color / (env_color + 1.0);
	return mix(vec4<f32>(vec3<f32>(-in.local_pos.z), 1.0), vec4<f32>(env_color, 1.0), 1.0);
}
