[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};
[[group(0), binding(0)]]
var<uniform> uniforms: Uniforms;

struct Light {
	position: vec3<f32>;
	color: vec3<f32>;
	radius: f32;
	intensity: f32;
	enabled: u32;
};
[[block]] struct Lights {
	arr: array<Light>;
};
[[group(1), binding(2)]]
var<storage, read> lights: Lights;

struct VertexInput {
	[[builtin(instance_index)]] instance: u32;
	[[location(0)]] position: vec3<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] color: vec3<f32>;
};

[[stage(vertex)]]
fn main(
	model: VertexInput
) -> VertexOutput {
	var out: VertexOutput;
	let light = lights.arr[model.instance];
	out.clip_position =
		uniforms.view_proj * vec4<f32>(model.position * light.radius + light.position, 1.0);
	out.color = light.color;
	return out;
}

[[stage(fragment)]]
fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	return vec4<f32>(in.color, 1.0);
}
