struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] tex_coord: vec2<f32>;
};

[[stage(vertex)]]
fn main([[builtin(vertex_index)]] index: u32) -> VertexOutput {
	var out: VertexOutput;
	let x = f32((index & 2u) >> 1u) * 2.0;
	let y = f32((index & 1u) >> 0u) * 2.0;
	out.clip_position = vec4<f32>(x * 2.0 - 1.0, y * 2.0 - 1.0, 0.5, 1.0);
	out.tex_coord = vec2<f32>(x, 1.0 - y);
	return out;
}
