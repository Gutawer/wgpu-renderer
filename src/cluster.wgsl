struct ClusterAabb {
	[[align(16)]]
	min_point: vec3<f32>;
	[[align(16)]]
	max_point: vec3<f32>;
};
[[block]] struct Clusters {
	arr: array<ClusterAabb>;
};

[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};

let NUM_CLUSTERS_X: u32 = 16u;
let NUM_CLUSTERS_Y: u32 = 8u;
let NUM_CLUSTERS_Z: u32 = 24u;

[[group(0), binding(0)]]
var<uniform> uniforms: Uniforms;
[[group(0), binding(1)]]
var<storage, read_write> clusters: Clusters;

fn screen_to_view(screen: vec4<f32>) -> vec4<f32> {
	let tex_coord = screen.xy / vec2<f32>(uniforms.screen_dimensions);

	let clip = vec4<f32>(tex_coord.x * 2.0 - 1.0, -(tex_coord.y * 2.0 - 1.0), screen.z, screen.w);

	let view = uniforms.inv_proj * clip;
	let view = view / view.w;

	return view;
}

fn line_intersection_to_z_plane(a: vec3<f32>, b: vec3<f32>, z_distance: f32) -> vec3<f32> {
	let normal = vec3<f32>(0.0, 0.0, 1.0);
	let ab = b - a;

	let t = (z_distance - dot(normal, a)) / dot(normal, ab);

	let result = a + t * ab;

	return result;
}

[[stage(compute), workgroup_size(16, 8, 4)]]
fn main([[builtin(global_invocation_id)]] global_id: vec3<u32>) {
	if (global_id.x >= NUM_CLUSTERS_X || global_id.y >= NUM_CLUSTERS_Y || global_id.z >= NUM_CLUSTERS_Z) {
		return;
	}

	let eye_pos = vec3<f32>(0.0);

	let cluster_index =
		global_id.x +
		global_id.y * NUM_CLUSTERS_X +
		global_id.z * NUM_CLUSTERS_X * NUM_CLUSTERS_Y;
	
	let min_point_screen = vec4<f32>(
		vec2<f32>(global_id.xy) * uniforms.tile_sizes, 0.0, 1.0
	);
	let max_point_screen = vec4<f32>(
		(vec2<f32>(global_id.xy) + vec2<f32>(1.0)) * uniforms.tile_sizes, 0.0, 1.0
	);

	let min_point_view = screen_to_view(min_point_screen).xyz;
	let max_point_view = screen_to_view(max_point_screen).xyz;

	let cluster_near =
		-uniforms.z_near *
		pow(uniforms.z_far / uniforms.z_near, f32(global_id.z) / f32(NUM_CLUSTERS_Z));

	let cluster_far =
		-uniforms.z_near *
		pow(uniforms.z_far / uniforms.z_near, f32(global_id.z + 1u) / f32(NUM_CLUSTERS_Z));

	let min_near = line_intersection_to_z_plane(eye_pos, min_point_view, cluster_near);
	let min_far  = line_intersection_to_z_plane(eye_pos, min_point_view, cluster_far);
	let max_near = line_intersection_to_z_plane(eye_pos, max_point_view, cluster_near);
	let max_far  = line_intersection_to_z_plane(eye_pos, max_point_view, cluster_far);

	let min_aabb = min(min(min_near, min_far), min(max_near, max_far));
	let max_aabb = max(max(min_near, min_far), max(max_near, max_far));

	clusters.arr[cluster_index].min_point = min_aabb;
	clusters.arr[cluster_index].max_point = max_aabb;
}
