use super::{ArchiveFileReader, ArchiveFileReaderEnum};
use std::{
    fs::File,
    io::BufReader,
    path::{Path, PathBuf},
};

#[derive(thiserror::Error, Debug)]
pub enum ArchiveLoadFromError {
    #[error("archive index not found: {archive}")]
    ArchiveIndexNotFound { archive: usize },
    #[error("file load failed in archive {archive:?}: {path:?}")]
    FileLoadFailed { archive: String, path: String },
}

#[enum_dispatch::enum_dispatch]
trait Archive {
    fn name(&self) -> &str;
    fn get_base_path(&self) -> &Path;
    fn get_from_path(&self, path: &str) -> Option<ArchiveFileReader>;
}

#[enum_dispatch::enum_dispatch(Archive)]
#[cfg_attr(test, derive(PartialEq, Eq))]
#[derive(Debug)]
pub(super) enum ArchiveKind {
    FolderArchive,
}

#[cfg_attr(test, derive(PartialEq, Eq, Clone))]
#[derive(Debug)]
pub(super) struct FolderArchive {
    pub(super) name: String,
    pub(super) base: PathBuf,
}
impl Archive for FolderArchive {
    fn name(&self) -> &str {
        &self.name
    }

    fn get_base_path(&self) -> &Path {
        &self.base
    }

    fn get_from_path(&self, path: &str) -> Option<ArchiveFileReader> {
        let joined = self.base.join(path);
        let file = File::open(joined).ok()?;
        Some(ArchiveFileReader(ArchiveFileReaderEnum::File(
            BufReader::new(file),
        )))
    }
}

#[derive(thiserror::Error, Debug)]
pub enum ArchiveAddError {
    #[error("archive already exists: {0}")]
    #[allow(unused)]
    AlreadyExists(String),
    #[error("archive path couldn't be found: {0:?}")]
    NotFound(PathBuf),
    #[error("archive path type not supported: {0:?}")]
    NotSupported(PathBuf),
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct ArchiveHandle(pub usize);
#[cfg_attr(test, derive(PartialEq, Eq))]
#[derive(Debug)]
pub struct Archives {
    archives: Vec<ArchiveKind>,
}

impl Archives {
    pub(super) fn fetch(
        &self,
        archive: ArchiveHandle,
        path: &str,
    ) -> Result<ArchiveFileReader, ArchiveLoadFromError> {
        let root = &self
            .archives
            .get(archive.0)
            .ok_or(ArchiveLoadFromError::ArchiveIndexNotFound { archive: archive.0 })?;
        root.get_from_path(path)
            .ok_or_else(|| ArchiveLoadFromError::FileLoadFailed {
                archive: root.name().to_string(),
                path: path.to_string(),
            })
    }

    pub(super) fn new() -> Self {
        Self { archives: vec![] }
    }

    pub(super) fn add_archive(
        &mut self,
        path: impl AsRef<Path>,
    ) -> Result<ArchiveHandle, ArchiveAddError> {
        let path = path.as_ref();
        let archive = {
            let path = path
                .canonicalize()
                .map_err(|_| ArchiveAddError::NotFound(path.to_path_buf()))?;
            if path.is_dir() {
                let name = path.file_name().unwrap().to_string_lossy().to_string();
                ArchiveKind::FolderArchive(FolderArchive { base: path, name })
            } else {
                return Err(ArchiveAddError::NotSupported(path));
            }
        };
        self.archives.push(archive);
        Ok(ArchiveHandle(self.archives.len() - 1))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_filesystem_add() {
        let mut filesystem = Archives::new();
        let archive = FolderArchive {
            name: "base".to_string(),
            base: PathBuf::from("test_data/base").canonicalize().unwrap(),
        };
        filesystem.add_archive("test_data/base").unwrap();
        assert_eq!(
            filesystem,
            Archives {
                archives: vec![ArchiveKind::FolderArchive(archive)]
            }
        );
    }

    #[test]
    fn test_filesystem_folder_resolve() {
        use std::io::Read;

        let archive = FolderArchive {
            name: "base".to_string(),
            base: PathBuf::from("test_data/base").canonicalize().unwrap(),
        };
        let filesystem = Archives {
            archives: vec![ArchiveKind::FolderArchive(archive)],
        };
        let bytes = filesystem
            .fetch(ArchiveHandle(0), "file")
            .unwrap()
            .bytes()
            .collect::<Result<Vec<_>, _>>()
            .unwrap();
        assert_eq!(
            bytes,
            include_str!("../../test_data/base/file")
                .bytes()
                .collect::<Vec<_>>()
        );
    }
}
