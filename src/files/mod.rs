use std::{
    fs::File,
    io::{BufRead, BufReader, Cursor, Read, Seek},
};

pub use archives::ArchiveHandle;
use archives::*;

mod archives;

#[derive(Debug)]
enum ArchiveFileReaderEnum {
    File(BufReader<File>),
    Memory(Cursor<Vec<u8>>),
}

#[derive(Debug)]
pub struct ArchiveFileReader(ArchiveFileReaderEnum);
impl Read for ArchiveFileReader {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match &mut self.0 {
            ArchiveFileReaderEnum::File(m) => m.read(buf),
            ArchiveFileReaderEnum::Memory(m) => m.read(buf),
        }
    }
}
impl Seek for ArchiveFileReader {
    fn seek(&mut self, pos: std::io::SeekFrom) -> std::io::Result<u64> {
        match &mut self.0 {
            ArchiveFileReaderEnum::File(m) => m.seek(pos),
            ArchiveFileReaderEnum::Memory(m) => m.seek(pos),
        }
    }
}
impl BufRead for ArchiveFileReader {
    fn fill_buf(&mut self) -> std::io::Result<&[u8]> {
        match &mut self.0 {
            ArchiveFileReaderEnum::File(m) => m.fill_buf(),
            ArchiveFileReaderEnum::Memory(m) => m.fill_buf(),
        }
    }
    fn consume(&mut self, amt: usize) {
        match &mut self.0 {
            ArchiveFileReaderEnum::File(m) => m.consume(amt),
            ArchiveFileReaderEnum::Memory(m) => m.consume(amt),
        }
    }
}

mod data_url_parse {
    #[derive(pest_derive::Parser)]
    #[grammar = "dataurl.pest"]
    pub struct DataUrlParser;
}

#[derive(thiserror::Error, Debug)]
pub enum ParseDataUrlError {
    #[error("parser error: {0}")]
    ParserError(pest::error::Error<data_url_parse::Rule>),
    #[error("base64 decode error: {0}")]
    Base64Error(base64::DecodeError),
}

pub fn parse_data_url(s: &str) -> Result<(String, Vec<u8>), ParseDataUrlError> {
    use pest::Parser;

    let mut media_type = "text/plain";
    let mut is_base64 = false;
    let mut data = "";
    use data_url_parse::*;
    let parsed = DataUrlParser::parse(Rule::dataurl, s)
        .map_err(ParseDataUrlError::ParserError)?
        .next()
        .unwrap();
    for item in parsed.into_inner() {
        match item.as_rule() {
            Rule::mediatype => {
                for item in item.into_inner() {
                    match item.as_rule() {
                        Rule::mediatype_no_param => {
                            media_type = item.as_str();
                        }
                        Rule::parameter => (),
                        _ => unreachable!(),
                    }
                }
            }
            Rule::base64_marker => {
                is_base64 = true;
            }
            Rule::data => {
                data = item.as_str();
            }
            Rule::EOI | Rule::comma => (),
            _ => unreachable!(),
        }
    }
    let media_type = media_type.to_string();

    if is_base64 {
        base64::decode(data)
            .map(|d| (media_type, d))
            .map_err(ParseDataUrlError::Base64Error)
    } else {
        Ok((
            media_type,
            urlencoding::decode_binary(data.as_bytes()).into_owned(),
        ))
    }
}

pub struct FileManager {
    archives: archives::Archives,
}

#[derive(thiserror::Error, Debug)]
pub enum ResolveError {
    #[error("url parse error: {0}")]
    UrlParseError(url::ParseError),
    #[error("archive url had no host")]
    NoArchiveHost,
    #[error("invalid archive url path")]
    InvalidArchivePath,
    #[error("invalid archive url host")]
    InvalidArchiveHost,
    #[error("data url not cannot-be-a-base")]
    DataNotCannotBeABase,
    #[error("data url parse failed: {0}")]
    DataUrlParseFailed(ParseDataUrlError),
    #[error("archives load from error: {0}")]
    ArchivesLoadFromError(archives::ArchiveLoadFromError),
    #[error("unsupported scheme: {0}")]
    UnsupportedScheme(String),
}

#[derive(thiserror::Error, Debug)]
pub enum FileManagerNewError {
    #[error("unsupported archive type: {0:?}")]
    #[allow(unused)]
    UnsupportedType(std::path::PathBuf),
    #[error("archive add error: {0:?}")]
    ArchiveAddError(ArchiveAddError),
}

#[derive(Debug)]
pub struct FetchData {
    pub reader: ArchiveFileReader,
    pub mediatype: Option<String>,
}

impl FileManager {
    pub(crate) fn new(archive_paths: &[&std::path::Path]) -> Result<Self, FileManagerNewError> {
        let mut archives = archives::Archives::new();
        for &a in archive_paths {
            archives
                .add_archive(a)
                .map_err(FileManagerNewError::ArchiveAddError)?;
        }
        Ok(Self { archives })
    }

    pub fn resolve(
        calling_archive: archives::ArchiveHandle,
        path: &str,
    ) -> Result<url::Url, ResolveError> {
        let base_url = url::Url::parse(&format!("archive://{}/", calling_archive.0)).unwrap();
        base_url.join(path).map_err(ResolveError::UrlParseError)
    }

    #[allow(unused)]
    pub fn fetch_by_resolving(
        &self,
        calling_archive: archives::ArchiveHandle,
        path: &str,
    ) -> Result<FetchData, ResolveError> {
        let base_url = url::Url::parse(&format!("archive://{}/", calling_archive.0)).unwrap();
        let resolved = base_url.join(path).map_err(ResolveError::UrlParseError)?;
        self.fetch_with_absolute(&resolved)
    }

    pub fn fetch_with_absolute(&self, u: &url::Url) -> Result<FetchData, ResolveError> {
        match u.scheme() {
            "archive" => {
                let archive_host = u.host_str().ok_or(ResolveError::NoArchiveHost)?;
                let archive_number = archive_host
                    .parse::<usize>()
                    .map_err(|_| ResolveError::InvalidArchiveHost)?;
                let archive_handle = ArchiveHandle(archive_number as usize);

                let checked_segments = u
                    .path_segments()
                    .unwrap()
                    .filter(|s| !s.is_empty())
                    .try_fold(vec![], |mut stack, s| {
                        let s =
                            urlencoding::decode(s).map_err(|_| ResolveError::InvalidArchivePath)?;
                        if s.contains('*')
                            || s.contains(':')
                            || s.contains('>')
                            || s.contains('<')
                            || s.contains('/')
                            || s.contains('\\')
                        {
                            return Err(ResolveError::InvalidArchivePath);
                        }
                        use std::ops::Deref;
                        match s.deref() {
                            ".." => {
                                stack.pop();
                            }
                            "." => {}
                            s => {
                                stack.push(s.to_string());
                            }
                        }
                        Ok(stack)
                    })?;
                let path = checked_segments.join("/");

                let r = self
                    .archives
                    .fetch(archive_handle, &path)
                    .map_err(ResolveError::ArchivesLoadFromError)?;
                Ok(FetchData {
                    reader: r,
                    mediatype: None,
                })
            }
            "data" => {
                if !u.cannot_be_a_base() {
                    return Err(ResolveError::DataNotCannotBeABase);
                }
                let data_str = u.path();
                let (mediatype, data) =
                    parse_data_url(data_str).map_err(ResolveError::DataUrlParseFailed)?;
                Ok(FetchData {
                    reader: ArchiveFileReader(ArchiveFileReaderEnum::Memory(Cursor::new(data))),
                    mediatype: Some(mediatype),
                })
            }
            s => Err(ResolveError::UnsupportedScheme(s.to_string())),
        }
    }
}
