use wgpu::include_wgsl;

pub struct Shaders {
    pub depth: wgpu::ShaderModule,
    pub lighting: wgpu::ShaderModule,
    pub cluster: wgpu::ShaderModule,
    pub cluster_cull: wgpu::ShaderModule,
    pub light: wgpu::ShaderModule,
    pub cubemap: wgpu::ShaderModule,
    pub skybox: wgpu::ShaderModule,
    pub full_screen_vert: wgpu::ShaderModule,
    pub shadow: wgpu::ShaderModule,
    pub mipmap: wgpu::ShaderModule,
}

pub fn get_shaders(device_queue: crate::DeviceQueue) -> Shaders {
    Shaders {
        depth: device_queue
            .device
            .create_shader_module(&include_wgsl!("depth.wgsl")),
        lighting: device_queue
            .device
            .create_shader_module(&include_wgsl!("lighting.wgsl")),
        cluster: device_queue
            .device
            .create_shader_module(&include_wgsl!("cluster.wgsl")),
        cluster_cull: device_queue
            .device
            .create_shader_module(&include_wgsl!("cluster_cull.wgsl")),
        light: device_queue
            .device
            .create_shader_module(&include_wgsl!("light.wgsl")),
        cubemap: device_queue
            .device
            .create_shader_module(&include_wgsl!("cubemap.wgsl")),
        skybox: device_queue
            .device
            .create_shader_module(&include_wgsl!("skybox.wgsl")),
        full_screen_vert: device_queue
            .device
            .create_shader_module(&include_wgsl!("full_screen_vert.wgsl")),
        shadow: device_queue
            .device
            .create_shader_module(&include_wgsl!("shadow.wgsl")),
        mipmap: device_queue
            .device
            .create_shader_module(&include_wgsl!("mipmap.wgsl")),
    }
}
