use core::num::{NonZeroU32, NonZeroU64};
use std::sync::Arc;

use wgpu::util::DeviceExt;
use wgpu_profiler::wgpu_profiler;

use crate::model::Vertex;
use crate::simple_mesh::{DrawCubemapMesh, SimpleMesh};
use crate::texture::*;

pub struct Cubemap<const MIPS: usize> {
    pub texture: wgpu::Texture,
    pub full_view: wgpu::TextureView,
    pub face_views: [[wgpu::TextureView; 6]; MIPS],
    pub sampler: wgpu::Sampler,
}

fn array_from_range<const N: usize>(range: core::ops::Range<usize>) -> [usize; N] {
    let mut arr = [0; N];
    for (elem, val) in arr.iter_mut().zip(range) {
        *elem = val;
    }
    arr
}

impl<const MIPS: usize> Cubemap<MIPS> {
    pub fn new(label: &str, device_queue: crate::DeviceQueue, dim: u32) -> Self {
        let size = wgpu::Extent3d {
            width: dim,
            height: dim,
            depth_or_array_layers: 6,
        };

        let texture = device_queue
            .device
            .create_texture(&wgpu::TextureDescriptor {
                label: Some(label),
                size,
                mip_level_count: MIPS as u32,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Rgba16Float,
                usage: wgpu::TextureUsages::TEXTURE_BINDING
                    | wgpu::TextureUsages::RENDER_ATTACHMENT,
            });
        let full_view = texture.create_view(&wgpu::TextureViewDescriptor {
            dimension: Some(wgpu::TextureViewDimension::Cube),
            ..Default::default()
        });
        let face_views = array_from_range::<MIPS>(0..MIPS).map(|mip| {
            [0, 1, 2, 3, 4, 5].map(|face| {
                texture.create_view(&wgpu::TextureViewDescriptor {
                    dimension: Some(wgpu::TextureViewDimension::D2),
                    base_array_layer: face,
                    array_layer_count: Some(NonZeroU32::new(1).unwrap()),
                    base_mip_level: mip as u32,
                    mip_level_count: Some(NonZeroU32::new(1).unwrap()),
                    ..Default::default()
                })
            })
        });
        let sampler = device_queue
            .device
            .create_sampler(&wgpu::SamplerDescriptor {
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Linear,
                min_filter: wgpu::FilterMode::Linear,
                mipmap_filter: wgpu::FilterMode::Linear,
                ..Default::default()
            });
        Self {
            texture,
            full_view,
            face_views,
            sampler,
        }
    }
}

pub fn get_capture_view_projections_buffer(
    device_queue: crate::DeviceQueue,
) -> (wgpu::Buffer, u32) {
    let capture_projection = crate::constants::OPENGL_TO_WGPU_MATRIX
        * cgmath::perspective(cgmath::Deg(90.0), 1.0, 0.1, 1000.0);
    let (capture_view_projections, padded_size) = {
        let mat = cgmath::Matrix4::look_at_rh;
        crate::offset_pad::pad(
            &device_queue.device.limits(),
            [
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [1.0, 0.0, 0.0].into(),
                    [0.0, -1.0, 0.0].into(),
                ),
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [-1.0, 0.0, 0.0].into(),
                    [0.0, -1.0, 0.0].into(),
                ),
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [0.0, 1.0, 0.0].into(),
                    [0.0, 0.0, 1.0].into(),
                ),
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [0.0, -1.0, 0.0].into(),
                    [0.0, 0.0, -1.0].into(),
                ),
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [0.0, 0.0, 1.0].into(),
                    [0.0, -1.0, 0.0].into(),
                ),
                mat(
                    [0.0, 0.0, 0.0].into(),
                    [0.0, 0.0, -1.0].into(),
                    [0.0, -1.0, 0.0].into(),
                ),
            ]
            .map(|m| -> [[f32; 4]; 4] { (capture_projection * m).into() }),
        )
    };
    (
        device_queue
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Capture View Projections Buffer"),
                contents: &capture_view_projections,
                usage: wgpu::BufferUsages::UNIFORM,
            }),
        padded_size,
    )
}

const SPECULAR_MIPS: usize = 5;

pub struct CubemapHandler {
    model: Arc<SimpleMesh>,

    diffuse_convolution_pipeline: wgpu::RenderPipeline,
    diffuse_convolution_bind_group_layout: wgpu::BindGroupLayout,

    specular_convolution_pipeline: wgpu::RenderPipeline,
    specular_convolution_bind_group_layout: wgpu::BindGroupLayout,

    equi_pipeline: wgpu::RenderPipeline,
    equi_bind_group_layout: wgpu::BindGroupLayout,

    pub lut_texture: Texture,

    capture_view_projections_buffer: Arc<wgpu::Buffer>,
    proj_padded_size: u32,
}

impl CubemapHandler {
    pub fn new(
        shaders: &crate::shaders::Shaders,
        device_queue: crate::DeviceQueue,
        cube_mesh: Arc<SimpleMesh>,
        capture_view_projections_buffer: Arc<wgpu::Buffer>,
        proj_padded_size: u32,
    ) -> Self {
        let equi_bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Equi to Cubemap Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::VERTEX,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: true,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                view_dimension: wgpu::TextureViewDimension::D2,
                                sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler {
                                comparison: false,
                                filtering: true,
                            },
                            count: None,
                        },
                    ],
                });
        let equi_pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: None,
                    bind_group_layouts: &[&equi_bind_group_layout],
                    push_constant_ranges: &[],
                });
        let equi_pipeline =
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Equi To Cubemap Pipeline",
                device_queue,
                layout: &equi_pipeline_layout,
                color_formats: &[wgpu::TextureFormat::Rgba16Float],
                depth: None,
                cull_mode: None,
                vertex_layouts: &[crate::simple_mesh::SimpleVertex::desc()],
                vert: (&shaders.cubemap, "main"),
                frag: (&shaders.cubemap, "equi_sample"),
            });

        let diffuse_convolution_bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Cubemap Convolution Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::VERTEX,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: true,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                view_dimension: wgpu::TextureViewDimension::Cube,
                                sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler {
                                comparison: false,
                                filtering: true,
                            },
                            count: None,
                        },
                    ],
                });
        let diffuse_convolution_pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: None,
                    bind_group_layouts: &[&diffuse_convolution_bind_group_layout],
                    push_constant_ranges: &[],
                });
        let diffuse_convolution_pipeline =
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Diffuse Convolution Pipeline",
                device_queue,
                layout: &diffuse_convolution_pipeline_layout,
                color_formats: &[wgpu::TextureFormat::Rgba16Float],
                depth: None,
                cull_mode: None,
                vertex_layouts: &[crate::simple_mesh::SimpleVertex::desc()],
                vert: (&shaders.cubemap, "main"),
                frag: (&shaders.cubemap, "diffuse_convolution"),
            });

        let specular_convolution_bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Cubemap Convolution Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::VERTEX,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: true,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                view_dimension: wgpu::TextureViewDimension::Cube,
                                sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler {
                                comparison: false,
                                filtering: true,
                            },
                            count: None,
                        },
                    ],
                });
        let specular_convolution_pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: None,
                    bind_group_layouts: &[&specular_convolution_bind_group_layout],
                    push_constant_ranges: &[],
                });
        let specular_convolution_pipeline =
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Specular Convolution Pipeline",
                device_queue,
                layout: &specular_convolution_pipeline_layout,
                color_formats: &[wgpu::TextureFormat::Rgba16Float],
                depth: None,
                cull_mode: None,
                vertex_layouts: &[crate::simple_mesh::SimpleVertex::desc()],
                vert: (&shaders.cubemap, "main"),
                frag: (&shaders.cubemap, "specular_convolution"),
            });

        let lut_renderer_pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: None,
                    bind_group_layouts: &[],
                    push_constant_ranges: &[],
                });
        let lut_renderer = crate::full_screen_draw::FullScreenDrawer::new(
            device_queue,
            shaders,
            (&shaders.cubemap, "integrate_brdf_main"),
            &lut_renderer_pipeline_layout,
            &[wgpu::TextureFormat::Rg16Float],
            None,
        );
        let size = wgpu::Extent3d {
            width: 512,
            height: 512,
            depth_or_array_layers: 1,
        };
        let lut_texture = device_queue
            .device
            .create_texture(&wgpu::TextureDescriptor {
                label: Some("Specular LUT"),
                size,
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Rg16Float,
                usage: wgpu::TextureUsages::TEXTURE_BINDING
                    | wgpu::TextureUsages::RENDER_ATTACHMENT,
            });
        let lut_texture =
            Texture::from_wgpu_texture(lut_texture, Texture::default_sampler(device_queue));
        let mut encoder =
            device_queue
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Specular LUT Render Encoder"),
                });
        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Specular LUT Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: &lut_texture.view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });
            lut_renderer.set_pipeline(&mut render_pass);
            lut_renderer.render(&mut render_pass);
        }
        device_queue.queue.submit(Some(encoder.finish()));

        Self {
            model: cube_mesh,
            diffuse_convolution_pipeline,
            diffuse_convolution_bind_group_layout,
            specular_convolution_pipeline,
            specular_convolution_bind_group_layout,
            equi_pipeline,
            equi_bind_group_layout,
            capture_view_projections_buffer,
            proj_padded_size,
            lut_texture,
        }
    }

    pub fn make_cubemap(
        &self,
        device_queue: crate::DeviceQueue,
        texture: &Texture,
        dim: u32,
        label: &str,
    ) -> Cubemap<1> {
        let ret = Cubemap::<1>::new(label, device_queue, dim);
        let mut encoder =
            device_queue
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Cubemap Render Encoder"),
                });
        let bind_group = device_queue
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &self.equi_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                            buffer: &self.capture_view_projections_buffer,
                            offset: 0,
                            size: Some(
                                NonZeroU64::new(std::mem::size_of::<[[f32; 4]; 4]>() as u64)
                                    .unwrap() as wgpu::BufferSize,
                            ),
                        }),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&texture.view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::Sampler(&texture.sampler),
                    },
                ],
                label: Some(&format!("{} Equi to Cubemap Bind Group", label)),
            });
        wgpu_profiler!(
            &format!("making {} cubemap", label),
            device_queue.profiler.lock(),
            &mut encoder,
            device_queue.device,
            {
                for (i, view) in ret.face_views[0].iter().enumerate() {
                    let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                        label: Some("Cubemap Render Pass"),
                        color_attachments: &[wgpu::RenderPassColorAttachment {
                            view,
                            resolve_target: None,
                            ops: wgpu::Operations {
                                load: wgpu::LoadOp::Clear(wgpu::Color {
                                    r: 0.0,
                                    g: 0.0,
                                    b: 0.0,
                                    a: 1.0,
                                }),
                                store: true,
                            },
                        }],
                        depth_stencil_attachment: None,
                    });
                    wgpu_profiler!(
                        &format!("face {}", i),
                        device_queue.profiler.lock(),
                        &mut render_pass,
                        device_queue.device,
                        {
                            render_pass.set_pipeline(&self.equi_pipeline);
                            render_pass.set_bind_group(
                                0,
                                &bind_group,
                                &[
                                    (i as u64 * self.proj_padded_size as u64)
                                        as wgpu::DynamicOffset,
                                ],
                            );
                            render_pass.draw_cubemap_mesh(&self.model, 0);
                        }
                    );
                }
            }
        );
        //device_queue.profiler.lock().resolve_queries(&mut encoder);
        device_queue.queue.submit(Some(encoder.finish()));
        ret
    }

    pub fn do_diffuse_convolution(
        &self,
        device_queue: crate::DeviceQueue,
        env: &Cubemap<1>,
        label: &str,
    ) -> Cubemap<1> {
        let ret = Cubemap::<1>::new(label, device_queue, 32);
        let mut encoder =
            device_queue
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Cubemap Render Encoder"),
                });
        let bind_group = device_queue
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &self.diffuse_convolution_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                            buffer: &self.capture_view_projections_buffer,
                            offset: 0,
                            size: Some(
                                NonZeroU64::new(std::mem::size_of::<[[f32; 4]; 4]>() as u64)
                                    .unwrap() as wgpu::BufferSize,
                            ),
                        }),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&env.full_view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::Sampler(&env.sampler),
                    },
                ],
                label: Some(&format!("{} Diffuse Convolution Bind Group", label)),
            });
        wgpu_profiler!(
            &format!("{} diffuse convolution", label),
            device_queue.profiler.lock(),
            &mut encoder,
            device_queue.device,
            {
                for (i, view) in ret.face_views[0].iter().enumerate() {
                    let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                        label: Some("Cubemap Render Pass"),
                        color_attachments: &[wgpu::RenderPassColorAttachment {
                            view,
                            resolve_target: None,
                            ops: wgpu::Operations {
                                load: wgpu::LoadOp::Clear(wgpu::Color {
                                    r: 0.0,
                                    g: 0.0,
                                    b: 0.0,
                                    a: 1.0,
                                }),
                                store: true,
                            },
                        }],
                        depth_stencil_attachment: None,
                    });
                    wgpu_profiler!(
                        &format!("face {}", i),
                        device_queue.profiler.lock(),
                        &mut render_pass,
                        device_queue.device,
                        {
                            render_pass.set_pipeline(&self.diffuse_convolution_pipeline);
                            render_pass.set_bind_group(
                                0,
                                &bind_group,
                                &[
                                    (i as u64 * self.proj_padded_size as u64)
                                        as wgpu::DynamicOffset,
                                ],
                            );
                            render_pass.draw_cubemap_mesh(&self.model, 0);
                        }
                    );
                }
            }
        );
        //device_queue.profiler.lock().resolve_queries(&mut encoder);
        device_queue.queue.submit(Some(encoder.finish()));
        ret
    }

    pub fn do_specular_convolution(
        &self,
        device_queue: crate::DeviceQueue,
        env: &Cubemap<1>,
        label: &str,
    ) -> Cubemap<SPECULAR_MIPS> {
        let ret = Cubemap::<SPECULAR_MIPS>::new(label, device_queue, 128);
        let mut encoder =
            device_queue
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Cubemap Render Encoder"),
                });
        let bind_group = device_queue
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &self.specular_convolution_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                            buffer: &self.capture_view_projections_buffer,
                            offset: 0,
                            size: Some(
                                NonZeroU64::new(std::mem::size_of::<[[f32; 4]; 4]>() as u64)
                                    .unwrap() as wgpu::BufferSize,
                            ),
                        }),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&env.full_view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::Sampler(&env.sampler),
                    },
                ],
                label: Some(&format!("{} Specular Convolution Bind Group", label)),
            });
        wgpu_profiler!(
            &format!("{} specular convolution", label),
            device_queue.profiler.lock(),
            &mut encoder,
            device_queue.device,
            {
                for (mip, views) in ret.face_views.iter().enumerate() {
                    wgpu_profiler!(
                        &format!("mip {}", mip),
                        device_queue.profiler.lock(),
                        &mut encoder,
                        device_queue.device,
                        {
                            for (face, view) in views.iter().enumerate() {
                                let mut render_pass =
                                    encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                                        label: Some("Cubemap Render Pass"),
                                        color_attachments: &[wgpu::RenderPassColorAttachment {
                                            view,
                                            resolve_target: None,
                                            ops: wgpu::Operations {
                                                load: wgpu::LoadOp::Clear(wgpu::Color {
                                                    r: 0.0,
                                                    g: 0.0,
                                                    b: 0.0,
                                                    a: 1.0,
                                                }),
                                                store: true,
                                            },
                                        }],
                                        depth_stencil_attachment: None,
                                    });
                                wgpu_profiler!(
                                    &format!("face {}", face),
                                    device_queue.profiler.lock(),
                                    &mut render_pass,
                                    device_queue.device,
                                    {
                                        render_pass
                                            .set_pipeline(&self.specular_convolution_pipeline);
                                        render_pass.set_bind_group(
                                            0,
                                            &bind_group,
                                            &[(face as u64 * self.proj_padded_size as u64)
                                                as wgpu::DynamicOffset],
                                        );
                                        render_pass.draw_cubemap_mesh(&self.model, mip as u32);
                                    }
                                );
                            }
                        }
                    );
                }
            }
        );
        //device_queue.profiler.lock().resolve_queries(&mut encoder);
        device_queue.queue.submit(Some(encoder.finish()));
        ret
    }
}

pub struct SkyboxRenderer {
    model: Arc<SimpleMesh>,
    pipeline: wgpu::RenderPipeline,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl SkyboxRenderer {
    pub fn new(
        device_queue: crate::DeviceQueue,
        shaders: &crate::shaders::Shaders,
        output_format: wgpu::TextureFormat,
        cube_mesh: Arc<SimpleMesh>,
    ) -> Self {
        let bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Skybox Renderer Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                view_dimension: wgpu::TextureViewDimension::Cube,
                                sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler {
                                comparison: false,
                                filtering: true,
                            },
                            count: None,
                        },
                    ],
                });
        let pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: None,
                    bind_group_layouts: &[&bind_group_layout],
                    push_constant_ranges: &[],
                });
        let pipeline =
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Skybox Renderer Pipeline",
                device_queue,
                layout: &pipeline_layout,
                color_formats: &[output_format],
                depth: Some((DepthTexture::DEPTH_FORMAT, wgpu::CompareFunction::LessEqual)),
                cull_mode: Some(wgpu::Face::Back),
                vertex_layouts: &[crate::simple_mesh::SimpleVertex::desc()],
                vert: (&shaders.skybox, "main"),
                frag: (&shaders.skybox, "main"),
            });
        Self {
            model: cube_mesh,
            pipeline,
            bind_group_layout,
        }
    }

    pub fn make_bind_group(
        &self,
        device_queue: crate::DeviceQueue,
        uniform_buffer: &wgpu::Buffer,
        cubemap: &Cubemap<1>,
        label: &str,
    ) -> wgpu::BindGroup {
        device_queue
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &self.bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                            buffer: uniform_buffer,
                            offset: 0,
                            size: None,
                        }),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::TextureView(&cubemap.full_view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::Sampler(&cubemap.sampler),
                    },
                ],
                label: Some(&format!("{} Skybox Bind Group", label)),
            })
    }

    pub fn render<'a, 'b: 'a>(
        &'b self,
        render_pass: &mut wgpu::RenderPass<'a>,
        cubemap_bind_group: &'b wgpu::BindGroup,
    ) {
        render_pass.set_pipeline(&self.pipeline);
        render_pass.set_bind_group(0, cubemap_bind_group, &[]);
        render_pass.draw_cubemap_mesh(&self.model, 0);
    }
}
