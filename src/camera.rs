use std::collections::HashMap;

use cgmath::prelude::*;
use winit::event::*;

#[readonly::make]
pub struct Camera {
    pub pos: cgmath::Point3<f32>,
    pub yaw: f32,
    pub pitch: f32,

    pub aspect: f32,
    pub fovy: f32,
    pub z_near: f32,
    pub z_far: f32,

    pub rot: cgmath::Matrix3<f32>,
    pub view: cgmath::Matrix4<f32>,
    pub proj: cgmath::Matrix4<f32>,
    pub inv_proj: cgmath::Matrix4<f32>,
    pub view_proj: cgmath::Matrix4<f32>,
}

impl Camera {
    pub fn new(
        pos: cgmath::Point3<f32>,
        yaw: f32,
        pitch: f32,
        aspect: f32,
        fovy: f32,
        z_near: f32,
        z_far: f32,
    ) -> Self {
        let mut ret = Self {
            pos,
            yaw,
            pitch,
            aspect,
            fovy,
            z_near,
            z_far,
            rot: cgmath::Matrix3::zero(),
            view: cgmath::Matrix4::zero(),
            proj: cgmath::Matrix4::zero(),
            inv_proj: cgmath::Matrix4::zero(),
            view_proj: cgmath::Matrix4::zero(),
        };
        ret.calc_proj();
        ret.calc_view();
        ret
    }

    pub fn set_aspect(&mut self, aspect: f32) {
        self.aspect = aspect;
        self.calc_proj();
    }

    fn calc_proj(&mut self) {
        let proj =
            cgmath::perspective(cgmath::Deg(self.fovy), self.aspect, self.z_near, self.z_far);
        self.proj = crate::constants::OPENGL_TO_WGPU_MATRIX * proj;
        self.inv_proj = self.proj.invert().unwrap();
        self.view_proj = self.proj * self.view;
    }

    fn calc_view(&mut self) {
        self.rot = cgmath::Matrix3::from_angle_z(cgmath::Rad(self.yaw))
            * cgmath::Matrix3::from_angle_y(cgmath::Rad(self.pitch));
        let view = cgmath::Matrix4::from_angle_y(cgmath::Rad(-self.pitch))
            * cgmath::Matrix4::from_angle_z(cgmath::Rad(-self.yaw))
            * cgmath::Matrix4::from_translation(-self.pos.to_vec());
        self.view = crate::constants::APP_TO_OPENGL_MATRIX * view;
        self.view_proj = self.proj * self.view;
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
enum Key {
    W,
    A,
    S,
    D,
    Left,
    Right,
    Up,
    Down,
    Space,
    Shift,
}

impl Key {
    fn from_winit_key(k: &VirtualKeyCode) -> Option<Self> {
        Some(match k {
            VirtualKeyCode::W => Key::W,
            VirtualKeyCode::A => Key::A,
            VirtualKeyCode::S => Key::S,
            VirtualKeyCode::D => Key::D,

            VirtualKeyCode::Left => Key::Left,
            VirtualKeyCode::Right => Key::Right,
            VirtualKeyCode::Up => Key::Up,
            VirtualKeyCode::Down => Key::Down,

            VirtualKeyCode::Space => Key::Space,
            VirtualKeyCode::LShift => Key::Shift,

            _ => {
                return None;
            }
        })
    }
}

pub struct CameraController {
    speed: f32,
    pressed_map: HashMap<Key, bool>,
}

impl CameraController {
    pub fn new(speed: f32) -> Self {
        Self {
            speed,
            pressed_map: HashMap::new(),
        }
    }

    pub fn process_window_events(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                ..
            } => {
                let is_pressed = *state == ElementState::Pressed;
                if let Some(k) = Key::from_winit_key(keycode) {
                    self.pressed_map.insert(k, is_pressed);
                    true
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    fn pressed(&self, key: Key) -> bool {
        *self.pressed_map.get(&key).unwrap_or(&false)
    }

    pub fn update_camera(&self, camera: &mut Camera, (mouse_dx, mouse_dy): (f32, f32), dt: f32) {
        camera.yaw -= mouse_dx * 0.0008;
        camera.pitch += mouse_dy * 0.0008;

        if self.pressed(Key::Left) {
            camera.yaw += self.speed * 1.0 / (2.0 * std::f32::consts::TAU) * dt;
        }
        if self.pressed(Key::Right) {
            camera.yaw -= self.speed * 1.0 / (2.0 * std::f32::consts::TAU) * dt;
        }
        if self.pressed(Key::Up) {
            camera.pitch -= self.speed * 1.0 / (2.0 * std::f32::consts::TAU) * dt;
        }
        if self.pressed(Key::Down) {
            camera.pitch += self.speed * 1.0 / (2.0 * std::f32::consts::TAU) * dt;
        }
        camera.pitch = camera
            .pitch
            .clamp(-std::f32::consts::TAU / 4.0, std::f32::consts::TAU / 4.0);

        if self.pressed(Key::Space) {
            camera.pos.z += self.speed * dt;
        }
        if self.pressed(Key::Shift) {
            camera.pos.z -= self.speed * dt;
        }

        if self.pressed(Key::W) {
            camera.pos += camera.rot * cgmath::Vector3::new(1.0, 0.0, 0.0) * self.speed * dt;
        }
        if self.pressed(Key::S) {
            camera.pos -= camera.rot * cgmath::Vector3::new(1.0, 0.0, 0.0) * self.speed * dt;
        }
        if self.pressed(Key::A) {
            camera.pos += camera.rot * cgmath::Vector3::new(0.0, 1.0, 0.0) * self.speed * dt;
        }
        if self.pressed(Key::D) {
            camera.pos -= camera.rot * cgmath::Vector3::new(0.0, 1.0, 0.0) * self.speed * dt;
        }
        camera.calc_view();
    }
}
