struct InstanceInput {
	[[location(5)]] model_matrix_0: vec4<f32>;
	[[location(6)]] model_matrix_1: vec4<f32>;
	[[location(7)]] model_matrix_2: vec4<f32>;
	[[location(8)]] model_matrix_3: vec4<f32>;

	[[location(9)]]  normal_matrix_0: vec3<f32>;
	[[location(10)]] normal_matrix_1: vec3<f32>;
	[[location(11)]] normal_matrix_2: vec3<f32>;
};

[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};
[[group(1), binding(0)]]
var<uniform> uniforms: Uniforms;

struct VertexInput {
	[[location(0)]] position: vec3<f32>;
	[[location(1)]] tex_coords: vec2<f32>;
	[[location(2)]] normal: vec3<f32>;
	[[location(3)]] tangent: vec4<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
};

[[stage(vertex)]]
fn main(
	model: VertexInput,
	instance: InstanceInput
) -> VertexOutput {
	let model_matrix = mat4x4<f32>(
		instance.model_matrix_0,
		instance.model_matrix_1,
		instance.model_matrix_2,
		instance.model_matrix_3,
	);
	let world_position = model_matrix * vec4<f32>(model.position, 1.0);

	var out: VertexOutput;
	out.clip_position = uniforms.view_proj * world_position;
	return out;
}

[[stage(fragment)]]
fn main(in: VertexOutput) {}
