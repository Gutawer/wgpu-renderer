#[derive(Debug, Clone)]
pub struct PointLight {
    pub position: cgmath::Point3<f32>,
    pub color: palette::rgb::LinSrgb,
    pub radius: f32,
    pub luminous_flux: f32,
    pub enabled: bool,
}

impl PointLight {
    pub fn to_gpu(&self) -> GPUPointLight {
        let (r, g, b) = self.color.into_components();
        GPUPointLight {
            position: self.position.into(),
            _padding0: 0,
            color: [r, g, b],
            radius: self.radius,
            intensity: self.luminous_flux / (2.0 * std::f32::consts::TAU),
            enabled: if self.enabled { 1 } else { 0 },
            _padding1: [0, 0],
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct GPUPointLight {
    position: [f32; 3],
    _padding0: u32,
    color: [f32; 3],
    radius: f32,
    intensity: f32,
    enabled: u32,
    _padding1: [u32; 2],
}
