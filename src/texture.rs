use core::num::NonZeroU32;
use std::collections::HashMap;
use std::path::Path;

use anyhow::*;
use image::GenericImageView;
use parking_lot::RwLock;
use wgpu::util::DeviceExt;
use wgpu_profiler::wgpu_profiler;

use crate::shaders::Shaders;

#[derive(Debug)]
pub struct TextureManager {
    mipmapper: TextureMipmapper,
    textures: RwLock<TextureList>,
}
crate::manager::manager!(
    TextureManager,
    TextureList,
    textures,
    TextureHandle,
    Texture
);

impl TextureManager {
    pub fn new(mipmapper: TextureMipmapper) -> Self {
        Self {
            mipmapper,
            textures: RwLock::new(TextureList(vec![])),
        }
    }

    pub fn add_from_path<P: AsRef<Path>>(
        &self,
        device_queue: crate::DeviceQueue,
        path: P,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
    ) -> Result<TextureHandle> {
        let texture = Texture::load(
            device_queue,
            path,
            srgb,
            sampler,
            needs_mipmaps,
            &self.mipmapper,
        )?;
        Ok(self.add(texture))
    }

    #[allow(unused)]
    pub fn add_from_bytes(
        &self,
        device_queue: crate::DeviceQueue,
        bytes: &[u8],
        label: &str,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
    ) -> Result<TextureHandle> {
        let texture = Texture::from_img_bytes(
            device_queue,
            bytes,
            label,
            srgb,
            sampler,
            needs_mipmaps,
            &self.mipmapper,
        )?;
        Ok(self.add(texture))
    }

    #[allow(unused)]
    pub fn add_from_image(
        &self,
        device_queue: crate::DeviceQueue,
        img: &image::DynamicImage,
        label: &str,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
    ) -> TextureHandle {
        let texture = Texture::from_image(
            device_queue,
            img,
            label,
            srgb,
            sampler,
            needs_mipmaps,
            &self.mipmapper,
        );
        self.add(texture)
    }

    pub fn add_from_raw_pixel_bytes(
        &self,
        device_queue: crate::DeviceQueue,
        pixels: Pixels,
        dims: (u32, u32),
        label: &str,
        format: wgpu::TextureFormat,
        sampler: wgpu::Sampler,
    ) -> TextureHandle {
        let texture = Texture::from_raw_pixel_bytes(
            device_queue,
            pixels,
            dims,
            label,
            format,
            sampler,
            &self.mipmapper,
        );
        self.add(texture)
    }
}

#[derive(Debug)]
pub struct TextureMipmapper {
    bind_group_layout: wgpu::BindGroupLayout,
    drawers: HashMap<wgpu::TextureFormat, crate::full_screen_draw::FullScreenDrawer>,
}

impl TextureMipmapper {
    pub const MIPMAPPABLE_FORMATS: &'static [wgpu::TextureFormat] = &[
        wgpu::TextureFormat::Rgba8Unorm,
        wgpu::TextureFormat::Rgba8UnormSrgb,
    ];

    pub fn new(device_queue: crate::DeviceQueue, shaders: &Shaders) -> Self {
        let bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Mipmapper Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                sample_type: wgpu::TextureSampleType::Float { filterable: true },
                                view_dimension: wgpu::TextureViewDimension::D2,
                                multisampled: false,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler {
                                filtering: true,
                                comparison: false,
                            },
                            count: None,
                        },
                    ],
                });
        let pipeline_layout =
            device_queue
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("Mipmapper Pipeline Layout"),
                    bind_group_layouts: &[&bind_group_layout],
                    push_constant_ranges: &[],
                });
        let drawers = Self::MIPMAPPABLE_FORMATS
            .iter()
            .map(|f| {
                let drawer = {
                    crate::full_screen_draw::FullScreenDrawer::new(
                        device_queue,
                        shaders,
                        (&shaders.mipmap, "main"),
                        &pipeline_layout,
                        &[*f],
                        None,
                    )
                };
                (*f, drawer)
            })
            .collect::<HashMap<_, _>>();
        Self {
            bind_group_layout,
            drawers,
        }
    }
}

#[derive(Debug)]
pub struct Texture {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
}

#[allow(clippy::enum_variant_names)]
pub enum Pixels<'a> {
    NoMipmaps(&'a [u8]),
    WithMipmaps(&'a [u8]),
    NeedsMipmaps(&'a [u8]),
}

impl Texture {
    fn load<P: AsRef<Path>>(
        device_queue: crate::DeviceQueue,
        path: P,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
        mipmapper: &TextureMipmapper,
    ) -> Result<Self> {
        let path_copy = path.as_ref().to_path_buf();
        let label = path_copy.to_string_lossy();

        let img = image::open(path)?;
        Ok(Self::from_image(
            device_queue,
            &img,
            &label,
            srgb,
            sampler,
            needs_mipmaps,
            mipmapper,
        ))
    }

    fn from_img_bytes(
        device_queue: crate::DeviceQueue,
        bytes: &[u8],
        label: &str,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
        mipmapper: &TextureMipmapper,
    ) -> Result<Self> {
        let img = image::load_from_memory(bytes)?;
        Ok(Self::from_image(
            device_queue,
            &img,
            label,
            srgb,
            sampler,
            needs_mipmaps,
            mipmapper,
        ))
    }

    fn from_image(
        device_queue: crate::DeviceQueue,
        img: &image::DynamicImage,
        label: &str,
        srgb: bool,
        sampler: wgpu::Sampler,
        needs_mipmaps: bool,
        mipmapper: &TextureMipmapper,
    ) -> Self {
        let rgba = img.to_rgba8();
        let (width, height) = img.dimensions();

        let pixels = if needs_mipmaps {
            Pixels::NeedsMipmaps(&rgba)
        } else {
            Pixels::NoMipmaps(&rgba)
        };

        Self::from_raw_pixel_bytes(
            device_queue,
            pixels,
            (width, height),
            label,
            if srgb {
                wgpu::TextureFormat::Rgba8UnormSrgb
            } else {
                wgpu::TextureFormat::Rgba8Unorm
            },
            sampler,
            mipmapper,
        )
    }

    fn from_raw_pixel_bytes(
        device_queue: crate::DeviceQueue,
        pixels: Pixels,
        (width, height): (u32, u32),
        label: &str,
        format: wgpu::TextureFormat,
        sampler: wgpu::Sampler,
        mipmapper: &TextureMipmapper,
    ) -> Self {
        let size = wgpu::Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        };

        const fn num_bits<T>() -> usize {
            std::mem::size_of::<T>() * 8
        }
        fn log2(x: u32) -> u32 {
            assert!(x > 0);
            num_bits::<u32>() as u32 - x.leading_zeros() - 1
        }

        let mip_level_count = match &pixels {
            Pixels::NoMipmaps(_) => 1,
            Pixels::NeedsMipmaps(_) => log2(u32::max(width, height)) + 1,
            Pixels::WithMipmaps(_) => log2(u32::max(width, height)) + 1,
        };

        let usage = wgpu::TextureUsages::COPY_DST | wgpu::TextureUsages::TEXTURE_BINDING;
        let usage = if let Pixels::NeedsMipmaps(_) = &pixels {
            usage | wgpu::TextureUsages::RENDER_ATTACHMENT
        } else {
            usage
        };

        let descriptor = &wgpu::TextureDescriptor {
            label: Some(label),
            size,
            mip_level_count,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage,
        };
        let texture = match &pixels {
            Pixels::NoMipmaps(p) => {
                device_queue
                    .device
                    .create_texture_with_data(device_queue.queue, descriptor, p)
            }
            Pixels::NeedsMipmaps(p) => {
                let texture = device_queue.device.create_texture(descriptor);
                device_queue.queue.write_texture(
                    wgpu::ImageCopyTexture {
                        texture: &texture,
                        mip_level: 0,
                        origin: wgpu::Origin3d::ZERO,
                        aspect: wgpu::TextureAspect::All,
                    },
                    p,
                    wgpu::ImageDataLayout {
                        offset: 0,
                        bytes_per_row: std::num::NonZeroU32::new(
                            format.describe().block_size as u32 * (width),
                        ),
                        rows_per_image: std::num::NonZeroU32::new(height),
                    },
                    size,
                );
                texture
            }
            Pixels::WithMipmaps(m) => {
                device_queue
                    .device
                    .create_texture_with_data(device_queue.queue, descriptor, m)
            }
        };

        if let Pixels::NeedsMipmaps(_) = pixels {
            let views = (0..mip_level_count)
                .map(|i| {
                    texture.create_view(&wgpu::TextureViewDescriptor {
                        dimension: Some(wgpu::TextureViewDimension::D2),
                        base_array_layer: 0,
                        array_layer_count: None,
                        base_mip_level: i as u32,
                        mip_level_count: Some(NonZeroU32::new(1).unwrap()),
                        ..Default::default()
                    })
                })
                .collect::<Vec<_>>();

            let drawer = &mipmapper.drawers.get(&format).unwrap();
            let mut encoder =
                device_queue
                    .device
                    .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                        label: Some("Mipmap Encoder"),
                    });
            wgpu_profiler!(
                &format!("{} mipmap generation", label),
                device_queue.profiler.lock(),
                &mut encoder,
                device_queue.device,
                {
                    for i in 1..mip_level_count {
                        let bind_group =
                            device_queue
                                .device
                                .create_bind_group(&wgpu::BindGroupDescriptor {
                                    label: Some(&format!("Mipmap {} Group", i)),
                                    layout: &mipmapper.bind_group_layout,
                                    entries: &[
                                        wgpu::BindGroupEntry {
                                            binding: 0,
                                            resource: wgpu::BindingResource::TextureView(
                                                &views[i as usize - 1],
                                            ),
                                        },
                                        wgpu::BindGroupEntry {
                                            binding: 1,
                                            resource: wgpu::BindingResource::Sampler(&sampler),
                                        },
                                    ],
                                });
                        let label = &format!("Mipmap {} Pass", i);
                        let mut render_pass =
                            encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                                label: Some(label),
                                color_attachments: &[wgpu::RenderPassColorAttachment {
                                    view: &views[i as usize],
                                    resolve_target: None,
                                    ops: wgpu::Operations {
                                        load: wgpu::LoadOp::Clear(wgpu::Color {
                                            r: 0.0,
                                            g: 0.0,
                                            b: 0.0,
                                            a: 0.0,
                                        }),
                                        store: true,
                                    },
                                }],
                                depth_stencil_attachment: None,
                            });
                        wgpu_profiler!(
                            &format!("mip {}", i),
                            device_queue.profiler.lock(),
                            &mut render_pass,
                            device_queue.device,
                            {
                                render_pass.set_bind_group(0, &bind_group, &[]);
                                drawer.set_pipeline(&mut render_pass);
                                drawer.render(&mut render_pass);
                            }
                        );
                    }
                }
            );
            //device_queue.profiler.lock().resolve_queries(&mut encoder);
            device_queue.queue.submit(Some(encoder.finish()));
        }

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());

        Self {
            texture,
            view,
            sampler,
        }
    }

    pub fn from_wgpu_texture(texture: wgpu::Texture, sampler: wgpu::Sampler) -> Self {
        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());

        Self {
            texture,
            view,
            sampler,
        }
    }

    pub fn default_sampler(device_queue: crate::DeviceQueue) -> wgpu::Sampler {
        device_queue
            .device
            .create_sampler(&wgpu::SamplerDescriptor {
                address_mode_u: wgpu::AddressMode::Repeat,
                address_mode_v: wgpu::AddressMode::Repeat,
                address_mode_w: wgpu::AddressMode::Repeat,
                mag_filter: wgpu::FilterMode::Linear,
                min_filter: wgpu::FilterMode::Linear,
                mipmap_filter: wgpu::FilterMode::Nearest,
                ..Default::default()
            })
    }
}

pub struct DepthTexture {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub depth_sampler: wgpu::Sampler,
    pub comp_sampler: wgpu::Sampler,
}

impl DepthTexture {
    pub const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

    pub fn create_depth_texture(
        device_queue: crate::DeviceQueue,
        width: u32,
        height: u32,
        label: &str,
        usage: wgpu::TextureUsages,
    ) -> Self {
        let size = wgpu::Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        };
        let desc = wgpu::TextureDescriptor {
            label: Some(label),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: Self::DEPTH_FORMAT,
            usage,
        };
        let texture = device_queue.device.create_texture(&desc);

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let depth_sampler = device_queue
            .device
            .create_sampler(&wgpu::SamplerDescriptor {
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Nearest,
                min_filter: wgpu::FilterMode::Nearest,
                mipmap_filter: wgpu::FilterMode::Nearest,
                compare: None,
                lod_min_clamp: -100.0,
                lod_max_clamp: 100.0,
                ..Default::default()
            });
        let comp_sampler = device_queue
            .device
            .create_sampler(&wgpu::SamplerDescriptor {
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Linear,
                min_filter: wgpu::FilterMode::Linear,
                mipmap_filter: wgpu::FilterMode::Nearest,
                compare: Some(wgpu::CompareFunction::LessEqual),
                lod_min_clamp: -100.0,
                lod_max_clamp: 100.0,
                ..Default::default()
            });

        Self {
            texture,
            view,
            depth_sampler,
            comp_sampler,
        }
    }
}
