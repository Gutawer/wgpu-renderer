struct InstanceInput {
	[[location(5)]] model_matrix_0: vec4<f32>;
	[[location(6)]] model_matrix_1: vec4<f32>;
	[[location(7)]] model_matrix_2: vec4<f32>;
	[[location(8)]] model_matrix_3: vec4<f32>;

	[[location(9)]]  normal_matrix_0: vec3<f32>;
	[[location(10)]] normal_matrix_1: vec3<f32>;
	[[location(11)]] normal_matrix_2: vec3<f32>;
};

[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};
[[group(1), binding(0)]]
var<uniform> uniforms: Uniforms;

struct Light {
	position: vec3<f32>;
	color: vec3<f32>;
	radius: f32;
	intensity: f32;
	enabled: u32;
};
[[block]] struct Lights {
	arr: array<Light>;
};

[[block]] struct LightIndices {
	count: atomic<u32>;
	arr: array<u32>;
};

struct LightGridEntry {
	offset: u32;
	count: u32;
};
[[block]] struct LightGrid {
	arr: array<LightGridEntry>;
};
[[group(2), binding(2)]]
var<storage, read> lights: Lights;
[[group(2), binding(3)]]
var<storage, read_write> light_indices: LightIndices;
[[group(2), binding(4)]]
var<storage, read_write> light_grid: LightGrid;

struct LightLocationEntry {
	positions: array<vec2<u32>, 6>;
	size: u32;
};
[[block]] struct LightLocations {
	arr: array<LightLocationEntry>;
};
[[group(2), binding(5)]]
var<storage, read> light_locations: LightLocations;

[[group(2), binding(6)]]
var t_shadow_map_comp: texture_depth_2d;
[[group(2), binding(7)]]
var s_shadow_map_comp: sampler_comparison;
[[group(2), binding(8)]]
var t_shadow_map_depth: texture_2d<f32>;
[[group(2), binding(9)]]
var s_shadow_map_depth: sampler;

struct VertexInput {
	[[location(0)]] position: vec3<f32>;
	[[location(1)]] tex_coords: vec2<f32>;
	[[location(2)]] normal: vec3<f32>;
	[[location(3)]] tangent: vec4<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] tex_coords: vec2<f32>;
	[[location(1)]] tbn_0: vec3<f32>;
	[[location(2)]] tbn_1: vec3<f32>;
	[[location(3)]] tbn_2: vec3<f32>;
	[[location(4)]] world_position: vec3<f32>;
};

[[stage(vertex)]]
fn main(
	model: VertexInput,
	instance: InstanceInput
) -> VertexOutput {
	let model_matrix = mat4x4<f32>(
		instance.model_matrix_0,
		instance.model_matrix_1,
		instance.model_matrix_2,
		instance.model_matrix_3,
	);
	let normal_matrix = mat3x3<f32>(
		instance.normal_matrix_0,
		instance.normal_matrix_1,
		instance.normal_matrix_2
	);
	
	let world_normal = normalize(normal_matrix * model.normal);
	let world_tangent = normalize(normal_matrix * model.tangent.xyz);
	let world_bitangent = cross(world_normal, world_tangent) * model.tangent.w;

	let world_position = model_matrix * vec4<f32>(model.position, 1.0);

	var out: VertexOutput;
	out.clip_position = uniforms.view_proj * world_position;
	out.tex_coords = model.tex_coords;
	out.world_position = world_position.xyz;
	out.tbn_0 = world_tangent;
	out.tbn_1 = world_bitangent;
	out.tbn_2 = world_normal;
	return out;
}

[[group(0), binding(0)]]
var t_albedo: texture_2d<f32>;
[[group(0), binding(1)]]
var s_albedo: sampler;

[[group(0), binding(2)]]
var t_normal: texture_2d<f32>;
[[group(0), binding(3)]]
var s_normal: sampler;

[[group(0), binding(4)]]
var t_metallic_roughness: texture_2d<f32>;
[[group(0), binding(5)]]
var s_metallic_roughness: sampler;

[[group(0), binding(6)]]
var t_ao: texture_2d<f32>;
[[group(0), binding(7)]]
var s_ao: sampler;

[[group(0), binding(8)]]
var t_emissive: texture_2d<f32>;
[[group(0), binding(9)]]
var s_emissive: sampler;

[[block]]
struct EmissiveFactor {
	factor: vec3<f32>;
};
[[group(0), binding(10)]]
var<uniform> emissive_factor: EmissiveFactor;

[[group(3), binding(0)]]
var t_irradiance: texture_cube<f32>;
[[group(3), binding(1)]]
var s_irradiance: sampler;
[[group(3), binding(2)]]
var t_prefilter: texture_cube<f32>;
[[group(3), binding(3)]]
var s_prefilter: sampler;
[[group(3), binding(4)]]
var t_specular_lut: texture_2d<f32>;
[[group(3), binding(5)]]
var s_specular_lut: sampler;

let PI: f32 = 3.14159265359;

fn proj(u: vec3<f32>, v: vec3<f32>) -> vec3<f32> {
	return u * dot(u, v) / dot(u, u);
}

fn fresnel_shlick(cos_theta: f32, f_0: vec3<f32>) -> vec3<f32> {
	return f_0 + (1.0 - f_0) * pow(max(1.0 - cos_theta, 0.0), 5.0);
}
fn fresnel_shlick_roughness(cos_theta: f32, f_0: vec3<f32>, roughness: f32) -> vec3<f32> {
	return f_0 + (max(vec3<f32>(1.0 - roughness), f_0) - f_0) * pow(clamp(1.0 - cos_theta, 0.0, 1.0), 5.0);
}

fn distribution_ggx(n: vec3<f32>, h: vec3<f32>, roughness: f32) -> f32 {
	let a = roughness * roughness;
	let a2 = a * a;
	let n_dot_h = max(dot(n, h), 0.0);
	let n_dot_h2 = n_dot_h * n_dot_h;

	let num = a2;
	let denom = n_dot_h2 * (a2 - 1.0) + 1.0;
	let denom = PI * denom * denom;

	return num / denom;
}
fn geometry_schlick_ggx(n_dot_v: f32, roughness: f32) -> f32 {
	let r = roughness + 1.0;
	let k = (r * r) / 8.0;

	let num = n_dot_v;
	let denom = n_dot_v * (1.0 - k) + k;

	return num / denom;
}
fn geometry_smith(n: vec3<f32>, v: vec3<f32>, l: vec3<f32>, roughness: f32) -> f32 {
	let n_dot_v = max(dot(n, v), 0.0);
	let n_dot_l = max(dot(n, l), 0.0);
	let ggx_2 = geometry_schlick_ggx(n_dot_v, roughness);
	let ggx_1 = geometry_schlick_ggx(n_dot_l, roughness);

	return ggx_1 * ggx_2;
}

fn linear_depth(depth_sample: f32) -> f32 {
	let depth_range = 2.0 * depth_sample - 1.0;
	return
		2.0 * uniforms.z_near * uniforms.z_far /
		(uniforms.z_far + uniforms.z_near - depth_range * (uniforms.z_far - uniforms.z_near));
}

struct CubeUvData {
	face: u32;
	uv: vec2<f32>;
};

fn cube_uv(v: vec3<f32>) -> CubeUvData {
	let v_abs = abs(v);
	var ma: f32;
	var uv: vec2<f32>;
	var face: u32;

	if (v_abs.z >= v_abs.x && v_abs.z >= v_abs.y) {
		ma = 0.5 / v_abs.z;
		if (v.z < 0.0) {
			face = 5u; uv = vec2<f32>(-v.x, -v.y);
		} else {
			face = 4u; uv = vec2<f32>(v.x, -v.y);
		}
	} elseif (v_abs.y >= v_abs.x) {
		ma = 0.5 / v_abs.y;
		if (v.y < 0.0) {
			face = 3u; uv = vec2<f32>(v.x, -v.z);
		} else {
			face = 2u; uv = vec2<f32>(v.x, v.z);
		}
	} else {
		ma = 0.5 / v_abs.x;
		if (v.x < 0.0) {
			face = 1u; uv = vec2<f32>(v.z, -v.y);
		} else {
			face = 0u; uv = vec2<f32>(-v.z, -v.y);
		}
	}
	return CubeUvData(face, vec2<f32>(uv.x * ma + 0.5, uv.y * ma + 0.5));
}

fn vogel_disk_sample(sample: u32, sample_count: u32, phi: f32) -> vec2<f32> {
	let GOLDEN_ANGLE = 2.4;

	let r = sqrt((f32(sample) + 0.5) / f32(sample_count));
	let theta = f32(sample) * GOLDEN_ANGLE + phi;

	let s = sin(theta);
	let c = cos(theta);

	return vec2<f32>(r * c, r * s);
}

fn interleaved_gradient_noise(p: vec2<f32>) -> f32 {
	let MAGIC = vec3<f32>(0.06711056, 0.00583715, 52.9829189);
	return fract(MAGIC.z * fract(dot(p, MAGIC.xy)));
}

struct BlockerResult {
	found: bool;
	average_blocker_depth: f32;
	num_blockers: u32;
};
fn blocker_search(
	base: CubeUvData, light_area: f32, sample_count: u32, ref: f32,
	atlas_size: vec2<f32>, face_size: f32, face_pos: vec2<f32>, noise: f32
) -> BlockerResult {
	var blocker_sum: f32 = 0.0;
	var num_blockers: u32 = 0u;
	for (var i: u32 = 0u; i < sample_count; i = i + 1u) {
		let offset = vogel_disk_sample(i, sample_count, noise) * light_area;
		let uv = base.uv + offset;
		let actual_uv = (face_pos + (face_size - 1.0) * uv + 0.5) / atlas_size;
		let shadow_map_depth = textureSample(t_shadow_map_depth, s_shadow_map_depth, actual_uv).r;
		if (uv.x >= 0.0 && uv.x <= 1.0 && uv.y >= 0.0 && uv.y <= 1.0) {
			if (shadow_map_depth < ref) {
				blocker_sum = blocker_sum + shadow_map_depth;
				num_blockers = num_blockers + 1u;
			}
		}
	}
	let average_blocker_depth = blocker_sum / f32(num_blockers);
	return BlockerResult(num_blockers >= 1u, average_blocker_depth, num_blockers);
}

fn pcss_samples(
	base: CubeUvData, ref: f32, filter_radius: f32, sample_count: u32,
	atlas_size: vec2<f32>, face_size: f32, face_pos: vec2<f32>, noise: f32
) -> f32 {
	var sum: f32 = 0.0;
	for (var i: u32 = 0u; i < sample_count; i = i + 1u) {
		let offset = vogel_disk_sample(i, sample_count, noise) * filter_radius;
		let uv = base.uv + offset;
		var sample_uv: vec2<f32>;
		if (uv.x >= 0.0 && uv.x <= 1.0 && uv.y >= 0.0 && uv.y <= 1.0) {
			sample_uv = uv;
		} else {
			sample_uv = base.uv;
		}
		let actual_uv = (face_pos + (face_size - 1.0) * sample_uv + 0.5) / atlas_size;
		let sample = textureSampleCompare(t_shadow_map_comp, s_shadow_map_comp, actual_uv, ref);
		sum = sum + sample;
	}
	return sum / f32(sample_count);
}

fn penumbra_size(receiver: f32, blocker: f32) -> f32 {
	return abs((receiver - blocker) / blocker);
}

fn sample_shadow_map(light_index: u32, sample_data: CubeUvData, ref: f32, noise: f32) -> f32 {
	let loc = light_locations.arr[light_index];

	let atlas_size = vec2<f32>(textureDimensions(t_shadow_map_comp));
	let size = f32(loc.size);
	let pos = vec2<f32>(light_locations.arr[light_index].positions[sample_data.face]);

	let light_area = 0.01;

	let sample_count_blocker = 8u;
	let sample_count_pcss = 16u;

	let blocker_result = blocker_search(
		sample_data, light_area + 0.000001, sample_count_blocker, ref, atlas_size, size, pos, noise
	);
	if (!blocker_result.found) {
		return 1.0;
	}

	let filter_size = light_area * penumbra_size(ref, blocker_result.average_blocker_depth);
	let filter_size = max(filter_size, 0.000001);

	return pcss_samples(sample_data, ref, filter_size, sample_count_pcss, atlas_size, size, pos, noise);
}

fn calculate_shadow(world_pos: vec3<f32>, world_normal: vec3<f32>, light_index: u32, noise: f32) -> f32 {
	let light = lights.arr[light_index];
	let loc = light_locations.arr[light_index];
	let size = f32(loc.size);

	let normal_offset_bias = 0.001;
	let light_to_frag = world_pos - light.position;
	let light_dir = normalize(light_to_frag);
	let light_to_frag = light_to_frag + world_normal * (1.0 - abs(dot(world_normal, light_dir))) * normal_offset_bias;

	let diff = length(light_to_frag);
	let sample_data = cube_uv(light_to_frag);
	let ref_depth = diff / light.radius - 0.02;
	let sample = sample_shadow_map(light_index, sample_data, ref_depth, noise);
	return sample;
}

[[stage(fragment)]]
fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	let vertex_normal = normalize(in.tbn_2);
	let tangent_to_world = mat3x3<f32>(in.tbn_0, in.tbn_1, in.tbn_2);
	let world_to_tangent = transpose(tangent_to_world);

	let tangent_position = world_to_tangent * in.world_position.xyz;
	let tangent_view_position = world_to_tangent * uniforms.view_pos.xyz;

	let object_albedo = textureSample(t_albedo, s_albedo, in.tex_coords).rgb;
	let object_normal = textureSample(t_normal, s_normal, in.tex_coords).xyz;
	let mr = textureSample(t_metallic_roughness, s_metallic_roughness, in.tex_coords);
	let object_metallic = mr.b;
	let object_roughness = mr.g;
	let object_ao = textureSample(t_ao, s_ao, in.tex_coords).r;
	let object_emissive = textureSample(t_emissive, s_emissive, in.tex_coords).rgb * emissive_factor.factor;

	let normal = normalize(object_normal * 2.0 - 1.0);
	let view_dir = normalize(tangent_view_position - tangent_position);

	let f_0 = vec3<f32>(0.04);
	let f_0 = mix(f_0, object_albedo, vec3<f32>(object_metallic));

	let scale = 24.0 / log2(uniforms.z_far / uniforms.z_near);
	let bias = -(24.0 * log2(uniforms.z_near) / log2(uniforms.z_far / uniforms.z_near));
	let z_tile = u32(max(log2(linear_depth(in.clip_position.z)) * scale + bias, 0.0));
	let tiles = vec3<u32>(vec2<u32>(in.clip_position.xy / uniforms.tile_sizes), z_tile);
	let tile_index = tiles.x + tiles.y * 16u + tiles.z * 16u * 8u;
	let light_count = light_grid.arr[tile_index].count;
	let light_offset = light_grid.arr[tile_index].offset;

	let noise = 2.0 * PI * interleaved_gradient_noise(in.clip_position.xy);

	//let shadow = calculate_shadow(in.world_position.xyz, vertex_normal, 0u, noise);
	//{
	//	return vec4<f32>(vec3<f32>(shadow), 1.0);
	//}

	var l_o: vec3<f32> = vec3<f32>(0.0);
	for (var i: u32 = 0u; i < light_count; i = i + 1u) {
		let light_index = light_indices.arr[light_offset + i];
		let light = lights.arr[light_index];
		if (light.enabled == 1u) {
			let shadow = calculate_shadow(in.world_position.xyz, vertex_normal, light_index, noise);

			if (shadow == 0.0) { continue; }

			let tangent_light_position = world_to_tangent * light.position;
			let light_dir = normalize(tangent_light_position - tangent_position);
			let half_dir = normalize(view_dir + light_dir);

			let diff = tangent_light_position - tangent_position;
			let dist_squared = dot(diff, diff);
			if (dist_squared <= light.radius * light.radius) {
				let inv_light_radius = 1.0 / light.radius;
				let factor = dist_squared * inv_light_radius * inv_light_radius;
				let smooth_factor = max(1.0 - factor * factor, 0.0);
				let attenuation = (smooth_factor * smooth_factor) / max(dist_squared, 1.0e-4);

				let radiance = light.color * light.intensity * attenuation;

				let ndf = distribution_ggx(normal, half_dir, object_roughness);
				let g = geometry_smith(normal, view_dir, light_dir, object_roughness);
				let f = fresnel_shlick(max(dot(half_dir, view_dir), 0.0), f_0);

				let numerator = ndf * g * f;
				let denominator =
					4.0 * max(dot(normal, view_dir), 0.0) * max(dot(normal, light_dir), 0.0) + 0.001;
				let specular = numerator / denominator;

				let k_s = f;
				let k_d = vec3<f32>(1.0) - k_s;
				let k_d = k_d * (1.0 - object_metallic);

				let n_dot_l = max(dot(normal, light_dir), 0.0);
				l_o = l_o + shadow * (k_d * object_albedo / PI + specular) * radiance * n_dot_l;
			}
		}
	}

	let world_view_dir = normalize(uniforms.view_pos.xyz - in.world_position.xyz);
	let world_normal = tangent_to_world * normal;
	let r = reflect(-world_view_dir, world_normal);

	let MAX_REFLECTION_LOD = 4.0;
	let prefiltered_color =
		textureSampleLevel(t_prefilter, s_prefilter, r, object_roughness * MAX_REFLECTION_LOD).rgb;

	let f = fresnel_shlick_roughness(max(dot(world_normal, world_view_dir), 0.0), f_0, object_roughness);
	let env_brdf = textureSample(
		t_specular_lut, s_specular_lut,
		vec2<f32>(max(dot(world_normal, world_view_dir), 0.0), object_roughness)
	).rg;
	let specular = prefiltered_color * (f * env_brdf.x + env_brdf.y);
	
	let k_s = fresnel_shlick_roughness(max(dot(normal, view_dir), 0.0), f_0, object_roughness);
	let k_d = 1.0 - k_s;
	let k_d = k_d * (1.0 - object_metallic);
	let irradiance = textureSample(t_irradiance, s_irradiance, world_normal).rgb;
	let diffuse = irradiance * object_albedo;
	let ambient = (k_d * diffuse + specular) * object_ao;

	let color = ambient + l_o + object_emissive;
	let color = color / (color + vec3<f32>(1.0));

	return vec4<f32>(color, 1.0);
}
