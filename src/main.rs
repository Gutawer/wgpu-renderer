#![allow(clippy::too_many_arguments)]

use std::sync::Arc;

use crate::model::Vertex;
use cgmath::prelude::*;
use itertools::Itertools;
use model::{DrawLight, DrawModel};
use palette::chromatic_adaptation::AdaptInto;
use parking_lot::Mutex;
use wgpu::util::DeviceExt;
use wgpu_profiler::*;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

const NUM_CLUSTERS_X: u32 = 16;
const NUM_CLUSTERS_Y: u32 = 8;
const NUM_CLUSTERS_Z: u32 = 24;

mod camera;
mod constants;
mod cubemap;
mod entity;
mod files;
mod full_screen_draw;
mod light;
mod manager;
mod model;
mod offset_pad;
mod pipeline;
mod resource_managers;
mod shaders;
mod shadow_map;
mod simple_mesh;
mod texture;

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Uniforms {
    inv_proj: [[f32; 4]; 4],
    view: [[f32; 4]; 4],
    proj: [[f32; 4]; 4],
    view_proj: [[f32; 4]; 4],
    view_pos: [f32; 4],
    screen_dimensions: [u32; 2],

    tile_size: [f32; 2],

    z_near: f32,
    z_far: f32,
}

impl Uniforms {
    fn new() -> Self {
        Self {
            view_pos: [0.0; 4],
            inv_proj: cgmath::Matrix4::identity().into(),
            view: cgmath::Matrix4::identity().into(),
            proj: cgmath::Matrix4::identity().into(),
            view_proj: cgmath::Matrix4::identity().into(),
            screen_dimensions: [0; 2],
            tile_size: [0.0; 2],
            z_near: 0.0,
            z_far: 0.0,
        }
    }

    fn update(&mut self, dimensions: (u32, u32), camera: &camera::Camera) {
        self.view_pos = camera.pos.to_homogeneous().into();
        self.view_proj = camera.view_proj.into();
        self.proj = camera.proj.into();

        self.screen_dimensions = [dimensions.0, dimensions.1];
        self.tile_size[0] = dimensions.0 as f32 / NUM_CLUSTERS_X as f32;
        self.tile_size[1] = dimensions.1 as f32 / NUM_CLUSTERS_Y as f32;

        self.view = camera.view.into();
        self.inv_proj = camera.inv_proj.into();
        self.z_near = camera.z_near;
        self.z_far = camera.z_far;
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub(crate) struct InstanceRaw {
    model: [[f32; 4]; 4],
    normal: [[f32; 3]; 3],
}

impl model::Vertex for InstanceRaw {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        const ATTRS: [wgpu::VertexAttribute; 7] = wgpu::vertex_attr_array![
            5 => Float32x4, 6 => Float32x4, 7 => Float32x4, 8 => Float32x4,
            9 => Float32x3, 10 => Float32x3, 11 => Float32x3
        ];
        use std::mem;

        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<InstanceRaw>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Instance,
            attributes: &ATTRS,
        }
    }
}

const NUM_INSTANCES_PER_ROW: u32 = 9;

#[derive(Clone, Copy)]
pub struct DeviceQueue<'a> {
    device: &'a wgpu::Device,
    queue: &'a wgpu::Queue,
    profiler: &'a Mutex<wgpu_profiler::GpuProfiler>,
}

struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    surface_conf: wgpu::SurfaceConfiguration,
    size: winit::dpi::PhysicalSize<u32>,
    render_pipeline: wgpu::RenderPipeline,

    depth_pass_pipeline: wgpu::RenderPipeline,

    camera: camera::Camera,
    camera_controller: camera::CameraController,

    uniforms: Uniforms,
    uniform_buffer: wgpu::Buffer,
    uniform_bind_group: wgpu::BindGroup,

    depth_texture: texture::DepthTexture,

    entity_manager: entity::EntityManager,
    resource_managers: resource_managers::ResourceManagers,

    base_entities: Vec<entity::EntityHandle>,

    light_mesh: model::MeshHandle,

    lights: Vec<light::PointLight>,
    light_buffer: wgpu::Buffer,
    light_render_pipeline: wgpu::RenderPipeline,

    cluster_bind_group: wgpu::BindGroup,
    cluster_pipeline: wgpu::ComputePipeline,
    cluster_cull_pipeline: wgpu::ComputePipeline,

    env_bind_group: wgpu::BindGroup,

    shadow_map_manager: shadow_map::ShadowMapManager,

    skybox_renderer: cubemap::SkyboxRenderer,
    cubemap_skybox_bind_group: wgpu::BindGroup,

    light_debug_render: bool,

    profiler: Mutex<GpuProfiler>,
    latest_profiler_results: Option<Vec<GpuTimerScopeResult>>,

    capturing_mouse: bool,
    accum_move: (f32, f32),

    needs_first_frame_profile: bool,
}

impl State {
    fn device_queue(&self) -> DeviceQueue<'_> {
        DeviceQueue {
            device: &self.device,
            queue: &self.queue,
            profiler: &self.profiler,
        }
    }

    async fn new(window: &Window) -> Self {
        let size = window.inner_size();

        let instance = wgpu::Instance::new(wgpu::Backends::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        async fn get_device(adapter: &wgpu::Adapter) -> (wgpu::Device, wgpu::Queue) {
            let r = adapter
                .request_device(
                    &wgpu::DeviceDescriptor {
                        features: wgpu::Features::TEXTURE_COMPRESSION_BC
                            | GpuProfiler::REQUIRED_WGPU_FEATURES,
                        limits: wgpu::Limits::default(),
                        label: None,
                    },
                    None,
                )
                .await;
            if let Ok((device, queue)) = r {
                return (device, queue);
            }
            let r = adapter
                .request_device(
                    &wgpu::DeviceDescriptor {
                        features: wgpu::Features::TEXTURE_COMPRESSION_ASTC_LDR
                            | GpuProfiler::REQUIRED_WGPU_FEATURES,
                        limits: wgpu::Limits::default(),
                        label: None,
                    },
                    None,
                )
                .await;
            if let Ok((device, queue)) = r {
                return (device, queue);
            }
            let r = adapter
                .request_device(
                    &wgpu::DeviceDescriptor {
                        features: GpuProfiler::REQUIRED_WGPU_FEATURES,
                        limits: wgpu::Limits::default(),
                        label: None,
                    },
                    None,
                )
                .await;
            r.unwrap()
        }

        let (device, queue) = get_device(&adapter).await;
        let profiler = Mutex::new(GpuProfiler::new(4, queue.get_timestamp_period()));

        let device_queue = DeviceQueue {
            device: &device,
            queue: &queue,
            profiler: &profiler,
        };

        let format = surface.get_preferred_format(&adapter).unwrap();
        let format = if format.describe().srgb {
            format
        } else {
            wgpu::TextureFormat::Bgra8UnormSrgb
        };

        let surface_conf = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        surface.configure(&device, &surface_conf);

        let shaders = shaders::get_shaders(device_queue);

        let depth_texture = texture::DepthTexture::create_depth_texture(
            device_queue,
            surface_conf.width,
            surface_conf.height,
            "depth_texture",
            wgpu::TextureUsages::RENDER_ATTACHMENT,
        );

        let texture_bind_group_layout = model::Material::get_bind_group_layout(&device);
        let env_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::Cube,
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: false,
                            filtering: true,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 2,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::Cube,
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 3,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: false,
                            filtering: true,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 4,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 5,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: false,
                            filtering: true,
                        },
                        count: None,
                    },
                ],
                label: Some("env_bind_group_layout"),
            });

        let count = 2;
        let lights = (0..count)
            .into_iter()
            .map(|i| {
                let ang = i as f32 * std::f32::consts::TAU / count as f32;
                let col = palette::Hsv::new(ang * 360.0 / std::f32::consts::TAU, 0.8, 1.0);
                light::PointLight {
                    position: (1.5 * (0.0 + ang.cos()), 1.5 * (0.0 + ang.sin()), 0.0).into(),
                    color: col.adapt_into(),
                    radius: 32.0,
                    luminous_flux: 2000.0,
                    enabled: true,
                }
            })
            .collect::<Vec<_>>();
        let gpu_lights = lights.iter().map(|x| x.to_gpu()).collect_vec();
        let light_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Light Buffer"),
            contents: bytemuck::cast_slice(&gpu_lights),
            usage: wgpu::BufferUsages::COPY_SRC
                | wgpu::BufferUsages::COPY_DST
                | wgpu::BufferUsages::STORAGE
                | wgpu::BufferUsages::UNIFORM,
        });

        let camera = camera::Camera::new(
            (0.0, 0.0, 0.0).into(),
            0.0,
            0.0,
            surface_conf.width as f32 / surface_conf.height as f32,
            45.0,
            0.1,
            1000.0,
        );
        let camera_controller = camera::CameraController::new(12.0);

        let mut uniforms = Uniforms::new();
        uniforms.update((surface_conf.width, surface_conf.height), &camera);

        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Uniform Buffer"),
            contents: bytemuck::cast_slice(&[uniforms]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });
        let uniform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("uniform_bind_group_layout"),
            });
        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &uniform_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: uniform_buffer.as_entire_binding(),
            }],
            label: Some("uniform_bind_group"),
        });

        let (capture_view_projections_buffer, proj_padded_size) =
            cubemap::get_capture_view_projections_buffer(device_queue);
        let capture_view_projections_buffer = Arc::new(capture_view_projections_buffer);

        let shadow_map_manager = shadow_map::ShadowMapManager::new(
            device_queue,
            &shaders,
            4096,
            capture_view_projections_buffer.clone(),
            proj_padded_size,
            lights.len(),
        );

        let cluster_output_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Cluster Output Buffer"),
            size: 4 * 8 * (NUM_CLUSTERS_X * NUM_CLUSTERS_Y * NUM_CLUSTERS_Z) as u64,
            usage: wgpu::BufferUsages::STORAGE,
            mapped_at_creation: false,
        });
        let cluster_light_grid_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Cluster Output Buffer"),
            size: 4 * 2 * (NUM_CLUSTERS_X * NUM_CLUSTERS_Y * NUM_CLUSTERS_Z) as u64,
            usage: wgpu::BufferUsages::STORAGE,
            mapped_at_creation: false,
        });
        let cluster_light_indices_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Cluster Output Buffer"),
            size: 4 * ((NUM_CLUSTERS_X * NUM_CLUSTERS_Y * NUM_CLUSTERS_Z * 128) as u64 + 1),
            usage: wgpu::BufferUsages::STORAGE,
            mapped_at_creation: false,
        });
        let cluster_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 2,
                        visibility: wgpu::ShaderStages::COMPUTE
                            | wgpu::ShaderStages::VERTEX
                            | wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: true },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 3,
                        visibility: wgpu::ShaderStages::COMPUTE | wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 4,
                        visibility: wgpu::ShaderStages::COMPUTE | wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 5,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: true },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 6,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Depth,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 7,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: true,
                            filtering: true,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 8,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 9,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            comparison: false,
                            filtering: false,
                        },
                        count: None,
                    },
                ],
                label: Some("cluster_bind_group_layout"),
            });
        let cluster_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &cluster_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: uniform_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: cluster_output_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: light_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: cluster_light_indices_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: cluster_light_grid_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 5,
                    resource: shadow_map_manager
                        .shadow_map_point_light_location_buffer
                        .as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 6,
                    resource: wgpu::BindingResource::TextureView(
                        &shadow_map_manager.shadow_map_texture.view,
                    ),
                },
                wgpu::BindGroupEntry {
                    binding: 7,
                    resource: wgpu::BindingResource::Sampler(
                        &shadow_map_manager.shadow_map_texture.comp_sampler,
                    ),
                },
                wgpu::BindGroupEntry {
                    binding: 8,
                    resource: wgpu::BindingResource::TextureView(
                        &shadow_map_manager.shadow_map_texture.view,
                    ),
                },
                wgpu::BindGroupEntry {
                    binding: 9,
                    resource: wgpu::BindingResource::Sampler(
                        &shadow_map_manager.shadow_map_texture.depth_sampler,
                    ),
                },
            ],
            label: Some("cluster_bind_group"),
        });

        let depth_pass_pipeline = {
            let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Depth Pass Pipeline Layout"),
                bind_group_layouts: &[
                    &texture_bind_group_layout,
                    &uniform_bind_group_layout,
                    &cluster_bind_group_layout,
                    &env_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });

            pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Depth Pre-pass Pipeline",
                device_queue,
                layout: &layout,
                color_formats: &[],
                depth: Some((
                    texture::DepthTexture::DEPTH_FORMAT,
                    wgpu::CompareFunction::LessEqual,
                )),
                cull_mode: Some(wgpu::Face::Back),
                vertex_layouts: &[model::RawModelVertex::desc(), InstanceRaw::desc()],
                vert: (&shaders.depth, "main"),
                frag: (&shaders.depth, "main"),
            })
        };
        let light_render_pipeline = {
            let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Light Pipeline Layout"),
                bind_group_layouts: &[&uniform_bind_group_layout, &cluster_bind_group_layout],
                push_constant_ranges: &[],
            });

            pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Light Debug Render Pipeline",
                device_queue,
                layout: &layout,
                color_formats: &[surface_conf.format],
                depth: Some((
                    texture::DepthTexture::DEPTH_FORMAT,
                    wgpu::CompareFunction::LessEqual,
                )),
                cull_mode: Some(wgpu::Face::Back),
                vertex_layouts: &[model::RawModelVertex::desc()],
                vert: (&shaders.light, "main"),
                frag: (&shaders.light, "main"),
            })
        };
        let render_pipeline = {
            let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[
                    &texture_bind_group_layout,
                    &uniform_bind_group_layout,
                    &cluster_bind_group_layout,
                    &env_bind_group_layout,
                ],
                push_constant_ranges: &[],
            });

            pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Main Render Pipeline",
                device_queue,
                layout: &layout,
                color_formats: &[surface_conf.format],
                depth: Some((
                    texture::DepthTexture::DEPTH_FORMAT,
                    wgpu::CompareFunction::Equal,
                )),
                cull_mode: Some(wgpu::Face::Back),
                vertex_layouts: &[model::RawModelVertex::desc(), InstanceRaw::desc()],
                vert: (&shaders.lighting, "main"),
                frag: (&shaders.lighting, "main"),
            })
        };
        let cluster_pipeline = {
            let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Cluster Pipeline Layout"),
                bind_group_layouts: &[&cluster_bind_group_layout],
                push_constant_ranges: &[],
            });
            device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                label: Some("Cluster Pipeline"),
                layout: Some(&layout),
                module: &shaders.cluster,
                entry_point: "main",
            })
        };
        let cluster_cull_pipeline = {
            let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Cluster Cull Pipeline Layout"),
                bind_group_layouts: &[&cluster_bind_group_layout],
                push_constant_ranges: &[],
            });
            device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                label: Some("Cluster Cull Pipeline"),
                layout: Some(&layout),
                module: &shaders.cluster_cull,
                entry_point: "main",
            })
        };

        let entity_manager = entity::EntityManager::new();

        let mipmapper = texture::TextureMipmapper::new(device_queue, &shaders);
        let resource_managers = resource_managers::ResourceManagers {
            textures: texture::TextureManager::new(mipmapper),
            materials: model::MaterialManager::new(),
            meshes: model::MeshManager::new(),
        };

        let res_dir = std::path::Path::new("res");

        let filesystem = files::FileManager::new(&["res".as_ref()]).unwrap();

        let draw_model = model::Model::load(
            device_queue,
            &texture_bind_group_layout,
            &filesystem,
            &resource_managers,
            crate::files::ArchiveHandle(0),
            "WaterBottle_Rotate.glb",
        )
        .unwrap();
        let light_mesh = model::Model::load(
            device_queue,
            &texture_bind_group_layout,
            &filesystem,
            &resource_managers,
            crate::files::ArchiveHandle(0),
            "light_debug.glb",
        )
        .unwrap()
        .meshes[0];

        const SPACE_BETWEEN: f32 = 6.0;
        let transforms = (0..NUM_INSTANCES_PER_ROW).flat_map(|y| {
            (0..NUM_INSTANCES_PER_ROW).flat_map(move |x| {
                let x = SPACE_BETWEEN * (x as f32 - (NUM_INSTANCES_PER_ROW - 1) as f32 / 2.0);
                let y = SPACE_BETWEEN * (y as f32 - (NUM_INSTANCES_PER_ROW - 1) as f32 / 2.0);

                let position = cgmath::Vector3 {
                    x,
                    y,
                    z: (x * x + y * y).sqrt() / 1.0 - 4.0,
                };

                let rotation = if position.is_zero() {
                    cgmath::Quaternion::from_axis_angle(cgmath::Vector3::unit_x(), cgmath::Deg(0.0))
                } else {
                    cgmath::Quaternion::from_axis_angle(position.normalize(), cgmath::Deg(0.0))
                };

                Some(entity::TransformData {
                    position,
                    rotation,
                    scale: cgmath::Vector3::new(20.0, 20.0, 20.0),
                })
            })
        });
        let mut base_entities = vec![];
        for t in transforms {
            let entity = entity::Entity::new(None, t);
            let entity = entity_manager.add(entity);
            base_entities.push(entity);
            draw_model.instantiate_parented_to(&entity_manager, entity);
        }

        let cube_mesh = simple_mesh::get_cube_mesh(device_queue);
        let cube_mesh = Arc::new(cube_mesh);

        let cubemap_handler = cubemap::CubemapHandler::new(
            &shaders,
            device_queue,
            cube_mesh.clone(),
            capture_view_projections_buffer,
            proj_padded_size,
        );
        let reader = std::io::BufReader::new(std::fs::File::open(res_dir.join("env.hdr")).unwrap());
        let hdr = image::codecs::hdr::HdrDecoder::new(reader).unwrap();
        let meta = hdr.metadata();
        let bytes = hdr.read_image_hdr().unwrap();
        let bytes = bytes
            .iter()
            .flat_map(|image::Rgb([r, g, b])| [*r, *g, *b, 1.0])
            .collect::<Vec<_>>();
        let env_texture = resource_managers.textures.add_from_raw_pixel_bytes(
            device_queue,
            texture::Pixels::NoMipmaps(bytemuck::cast_slice(&bytes)),
            (meta.width, meta.height),
            "Env HDR",
            wgpu::TextureFormat::Rgba32Float,
            texture::Texture::default_sampler(device_queue),
        );
        let cubemap = cubemap_handler.make_cubemap(
            device_queue,
            &resource_managers.textures.get(env_texture),
            meta.width.max(meta.height).min(8192),
            "Env Cubemap",
        );
        let irradiance_cubemap =
            cubemap_handler.do_diffuse_convolution(device_queue, &cubemap, "Diffuse Cubemap");
        let prefiltered_cubemap =
            cubemap_handler.do_specular_convolution(device_queue, &cubemap, "Specular Cubemap");

        let env_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &env_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&irradiance_cubemap.full_view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&irradiance_cubemap.sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::TextureView(&prefiltered_cubemap.full_view),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: wgpu::BindingResource::Sampler(&prefiltered_cubemap.sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: wgpu::BindingResource::TextureView(&cubemap_handler.lut_texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 5,
                    resource: wgpu::BindingResource::Sampler(&cubemap_handler.lut_texture.sampler),
                },
            ],
            label: Some("uniform_bind_group"),
        });

        let skybox_renderer =
            cubemap::SkyboxRenderer::new(device_queue, &shaders, surface_conf.format, cube_mesh);
        let cubemap_skybox_bind_group =
            skybox_renderer.make_bind_group(device_queue, &uniform_buffer, &cubemap, "Cubemap");
        //profiler.lock().end_frame().unwrap();
        queue.submit(None);

        Self {
            surface,
            device,
            queue,
            surface_conf,
            size,
            render_pipeline,
            depth_pass_pipeline,
            camera,
            camera_controller,
            uniforms,
            uniform_buffer,
            uniform_bind_group,
            depth_texture,
            light_mesh,
            lights,
            light_buffer,
            light_render_pipeline,
            cluster_bind_group,
            cluster_pipeline,
            cluster_cull_pipeline,
            profiler,
            latest_profiler_results: None,
            light_debug_render: false,
            skybox_renderer,
            cubemap_skybox_bind_group,
            env_bind_group,
            capturing_mouse: false,
            accum_move: (0.0, 0.0),
            shadow_map_manager,
            entity_manager,
            resource_managers,
            base_entities,
            needs_first_frame_profile: true,
        }
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.surface_conf.width = new_size.width;
            self.surface_conf.height = new_size.height;
            self.surface.configure(&self.device, &self.surface_conf);

            self.camera
                .set_aspect(self.surface_conf.width as f32 / self.surface_conf.height as f32);

            self.depth_texture = texture::DepthTexture::create_depth_texture(
                self.device_queue(),
                self.surface_conf.width,
                self.surface_conf.height,
                "depth_texture",
                wgpu::TextureUsages::RENDER_ATTACHMENT,
            );
        }
    }

    fn window_input(&mut self, window: &Window, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::MouseInput {
                state: ElementState::Pressed,
                button: MouseButton::Left,
                ..
            } if !self.capturing_mouse => {
                let _ = window.set_cursor_grab(true);
                window.set_cursor_visible(false);
                self.capturing_mouse = true;
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(k),
                        ..
                    },
                ..
            } => match k {
                VirtualKeyCode::G => {
                    if let Some(profile_data) = &self.latest_profiler_results {
                        wgpu_profiler::chrometrace::write_chrometrace(
                            std::path::Path::new("trace.json"),
                            profile_data,
                        )
                        .expect("Failed to write trace.json");
                        println!("Written");
                    } else {
                        println!("Failed to write because of lack of profiler results");
                    }
                    return true;
                }
                VirtualKeyCode::L => {
                    self.light_debug_render = !self.light_debug_render;
                    return true;
                }
                VirtualKeyCode::Escape if self.capturing_mouse => {
                    let _ = window.set_cursor_grab(false);
                    window.set_cursor_visible(true);
                    self.capturing_mouse = false;
                    return true;
                }
                _ => {}
            },
            _ => {}
        }
        self.camera_controller.process_window_events(event)
    }

    fn device_input(&mut self, event: &DeviceEvent) {
        match event {
            DeviceEvent::MouseMotion { delta } if self.capturing_mouse => {
                self.accum_move.0 += delta.0 as f32;
                self.accum_move.1 += delta.1 as f32;
            }
            _ => {}
        }
    }

    fn update(&mut self, dt: f32) {
        self.camera_controller
            .update_camera(&mut self.camera, self.accum_move, dt);
        self.accum_move = (0.0, 0.0);
        self.uniforms.update(
            (self.surface_conf.width, self.surface_conf.height),
            &self.camera,
        );
        self.queue.write_buffer(
            &self.uniform_buffer,
            0,
            bytemuck::cast_slice(&[self.uniforms]),
        );

        for h in self.base_entities.iter() {
            let mut e = self.entity_manager.get_mut(*h);
            let mut transform = e.transform.get_local();
            transform.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_z(),
                cgmath::Deg(60.0 * dt),
            ) * transform.rotation;
            e.transform.set_local(transform);
        }

        //for l in self.lights.iter_mut() {
        //	let old_position = l.position.to_vec();
        //	l.position =
        //		cgmath::Point3::from_vec(
        //			cgmath::Quaternion::from_axis_angle((0.0, 0.0, 1.0).into(), cgmath::Deg(10.0 * dt))
        //				* old_position
        //		);
        //}
        let gpu_lights = self.lights.iter().map(|x| x.to_gpu()).collect::<Vec<_>>();
        self.queue
            .write_buffer(&self.light_buffer, 0, bytemuck::cast_slice(&gpu_lights));
    }

    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        let device_queue = DeviceQueue {
            device: &self.device,
            queue: &self.queue,
            profiler: &self.profiler,
        };

        self.shadow_map_manager
            .calculate_light_shadow_map_locations(
                &self.lights,
                &self.camera,
                (self.surface_conf.width, self.surface_conf.height),
                device_queue,
            );

        entity::propagate_transforms(&self.entity_manager);
        entity::collect_instances(
            &self.entity_manager,
            &self.resource_managers.meshes,
            &device_queue,
        );

        let frame = self.surface.get_current_texture()?;

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        let all_meshes = self.resource_managers.meshes.get_all();
        let all_materials = self.resource_managers.materials.get_all();
        wgpu_profiler!(
            "depth pre-pass",
            self.profiler.lock(),
            &mut encoder,
            &self.device,
            {
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: Some("Depth Pre-Pass"),
                    color_attachments: &[],
                    depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                        view: &self.depth_texture.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Clear(1.0),
                            store: true,
                        }),
                        stencil_ops: None,
                    }),
                });

                render_pass.set_pipeline(&self.depth_pass_pipeline);
                for m in all_meshes.iter() {
                    let instances = if let Some(b) = &m.instances {
                        b
                    } else {
                        continue;
                    };
                    render_pass.set_vertex_buffer(1, instances.buffer.slice(..));
                    wgpu_profiler!(
                        &format!("{} instances", m.name),
                        self.profiler.lock(),
                        &mut render_pass,
                        &self.device,
                        {
                            render_pass.draw_mesh_instanced(
                                m,
                                &all_materials,
                                0..instances.num_instances,
                                &self.uniform_bind_group,
                                &self.cluster_bind_group,
                                &self.env_bind_group,
                            );
                        }
                    );
                }
            }
        );
        wgpu_profiler!(
            "compute",
            self.profiler.lock(),
            &mut encoder,
            &self.device,
            {
                let mut compute_pass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
                    label: Some("Compute Pass"),
                });

                wgpu_profiler!(
                    "cluster aabbs",
                    self.profiler.lock(),
                    &mut compute_pass,
                    &self.device,
                    {
                        compute_pass.set_pipeline(&self.cluster_pipeline);

                        compute_pass.set_bind_group(0, &self.cluster_bind_group, &[]);

                        compute_pass.dispatch(
                            NUM_CLUSTERS_X / 16,
                            NUM_CLUSTERS_Y / 8,
                            NUM_CLUSTERS_Z / 4,
                        );
                    }
                );

                wgpu_profiler!(
                    "cluster cull",
                    self.profiler.lock(),
                    &mut compute_pass,
                    &self.device,
                    {
                        compute_pass.set_pipeline(&self.cluster_cull_pipeline);

                        compute_pass.set_bind_group(0, &self.cluster_bind_group, &[]);

                        compute_pass.dispatch(1, 1, 6);
                    }
                );
            }
        );
        wgpu_profiler!(
            "shadow maps",
            self.profiler.lock(),
            &mut encoder,
            &self.device,
            {
                let instance_groups = all_meshes
                    .iter()
                    .filter_map(|m| {
                        m.instances
                            .as_ref()
                            .map(|instances| (m, &instances.buffer, instances.num_instances))
                    })
                    .collect_vec();
                self.shadow_map_manager.render_shadow_maps(
                    self.device_queue(),
                    &mut encoder,
                    &instance_groups,
                    &self.lights,
                    &self.light_buffer,
                );
            }
        );
        wgpu_profiler!(
            "rendering",
            self.profiler.lock(),
            &mut encoder,
            &self.device,
            {
                let view = frame
                    .texture
                    .create_view(&wgpu::TextureViewDescriptor::default());
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: Some("Main Render Pass"),
                    color_attachments: &[wgpu::RenderPassColorAttachment {
                        view: &view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Clear(wgpu::Color {
                                r: 0.1,
                                g: 0.2,
                                b: 0.3,
                                a: 1.0,
                            }),
                            store: true,
                        },
                    }],
                    depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                        view: &self.depth_texture.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        }),
                        stencil_ops: None,
                    }),
                });

                if self.light_debug_render {
                    wgpu_profiler!(
                        "light (debug)",
                        self.profiler.lock(),
                        &mut render_pass,
                        &self.device,
                        {
                            render_pass.set_pipeline(&self.light_render_pipeline);
                            render_pass.draw_light_meshes(
                                all_meshes.get(self.light_mesh),
                                &self.uniform_bind_group,
                                &self.cluster_bind_group,
                                self.lights.len() as u32,
                            );
                        }
                    );
                }

                render_pass.set_pipeline(&self.render_pipeline);
                for m in all_meshes.iter() {
                    let instances = if let Some(b) = &m.instances {
                        b
                    } else {
                        continue;
                    };
                    render_pass.set_vertex_buffer(1, instances.buffer.slice(..));
                    wgpu_profiler!(
                        &format!("{} instances", m.name),
                        self.profiler.lock(),
                        &mut render_pass,
                        &self.device,
                        {
                            render_pass.draw_mesh_instanced(
                                m,
                                &all_materials,
                                0..instances.num_instances,
                                &self.uniform_bind_group,
                                &self.cluster_bind_group,
                                &self.env_bind_group,
                            );
                        }
                    );
                }

                wgpu_profiler!(
                    "skybox",
                    self.profiler.lock(),
                    &mut render_pass,
                    &self.device,
                    {
                        self.skybox_renderer
                            .render(&mut render_pass, &self.cubemap_skybox_bind_group);
                    }
                );
            }
        );

        //self.profiler.lock().resolve_queries(&mut encoder);
        self.queue.submit(Some(encoder.finish()));
        frame.present();

        //self.profiler.lock().end_frame().unwrap();
        if let Some(results) = self.profiler.lock().process_finished_frame() {
            if self.needs_first_frame_profile {
                wgpu_profiler::chrometrace::write_chrometrace(
                    std::path::Path::new("trace_startup.json"),
                    &results,
                )
                .expect("Failed to write trace_startup.json");
                self.needs_first_frame_profile = false;
            }
            self.latest_profiler_results = Some(results);
        }

        Ok(())
    }
}

fn main() {
    env_logger::init();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut state = pollster::block_on(State::new(&window));
    let mut last_render_time = std::time::Instant::now();

    event_loop.run(move |event, _, control_flow| match event {
        Event::RedrawRequested(_) => {
            let now = std::time::Instant::now();
            let dt = (now - last_render_time).as_secs_f32();
            //println!("dt: {}ms", dt * 1000.0);
            last_render_time = now;
            state.update(dt);
            match state.render() {
                Ok(_) => {}
                Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            window.request_redraw();
        }
        Event::DeviceEvent { event, .. } => {
            state.device_input(&event);
        }
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => {
            if !state.window_input(&window, event) {
                match event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => {
                        *control_flow = ControlFlow::Exit;
                    }
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        state.resize(**new_inner_size);
                    }
                    _ => {}
                }
            }
        }
        _ => {}
    });
}
