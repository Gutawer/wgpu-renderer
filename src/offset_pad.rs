fn pad_one<P: bytemuck::Pod + bytemuck::Zeroable + Copy + Clone>(
    padded_size: u32,
    data: P,
) -> Vec<u8> {
    bytemuck::bytes_of(&data)
        .iter()
        .copied()
        .chain(std::iter::repeat(0))
        .take(padded_size as usize)
        .collect()
}

pub fn pad<P: bytemuck::Pod + bytemuck::Zeroable + Copy + Clone>(
    limits: &wgpu::Limits,
    data: impl IntoIterator<Item = P>,
) -> (Vec<u8>, u32) {
    use std::convert::TryInto;

    let min_binding_size = num::integer::lcm(
        limits.min_storage_buffer_offset_alignment,
        limits.min_uniform_buffer_offset_alignment,
    );
    let data_size: u32 = std::mem::size_of::<P>().try_into().unwrap();
    let padded_size = (min_binding_size..)
        .step_by(min_binding_size as usize)
        .find(|&m| m >= data_size)
        .unwrap();

    (
        data.into_iter()
            .flat_map(|p| pad_one(padded_size, p))
            .collect(),
        padded_size,
    )
}
