struct InstanceInput {
	[[location(5)]] model_matrix_0: vec4<f32>;
	[[location(6)]] model_matrix_1: vec4<f32>;
	[[location(7)]] model_matrix_2: vec4<f32>;
	[[location(8)]] model_matrix_3: vec4<f32>;

	[[location(9)]]  normal_matrix_0: vec3<f32>;
	[[location(10)]] normal_matrix_1: vec3<f32>;
	[[location(11)]] normal_matrix_2: vec3<f32>;
};

struct VertexInput {
	[[location(0)]] position: vec3<f32>;
	[[location(1)]] tex_coords: vec2<f32>;
	[[location(2)]] normal: vec3<f32>;
	[[location(3)]] tangent: vec4<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] local_pos: vec3<f32>;
};

[[block]]
struct ShadowViewProj {
	view_proj: mat4x4<f32>;
};
[[group(0), binding(0)]]
var<uniform> shadow_view_proj: ShadowViewProj;

[[block]]
struct Light {
	position: vec3<f32>;
	color: vec3<f32>;
	radius: f32;
	intensity: f32;
	enabled: u32;
};
[[group(0), binding(1)]]
var<uniform> light: Light;

[[stage(vertex)]]
fn main(
	model: VertexInput,
	instance: InstanceInput
) -> VertexOutput {
	let model_matrix = mat4x4<f32>(
		instance.model_matrix_0,
		instance.model_matrix_1,
		instance.model_matrix_2,
		instance.model_matrix_3,
	);
	let world_position = model_matrix * vec4<f32>(model.position, 1.0);

	var out: VertexOutput;
	out.clip_position = shadow_view_proj.view_proj * vec4<f32>(world_position.xyz - light.position, 1.0);
	out.clip_position.y = -out.clip_position.y;
	out.local_pos = world_position.xyz;
	return out;
}

[[stage(fragment)]]
fn main(in: VertexOutput) -> [[builtin(frag_depth)]] f32 {
	let dist = length(in.local_pos - light.position);
	let dist = clamp(dist / light.radius, 0.0, 1.0);
	return dist;
}

[[stage(fragment)]]
fn clear() -> [[builtin(frag_depth)]] f32 { return 1.0; }
