use std::collections::HashMap;

use crate::model::MeshHandle;

use itertools::Itertools;
use parking_lot::RwLock;

#[derive(Debug, Clone, Copy)]
pub struct TransformData {
    pub position: cgmath::Vector3<f32>,
    pub rotation: cgmath::Quaternion<f32>,
    pub scale: cgmath::Vector3<f32>,
}

#[derive(Debug, Clone, Copy)]
pub struct Transform {
    local: TransformData,
    global: cgmath::Matrix4<f32>,
    needs_update: bool,
    changed: bool,
}

impl Transform {
    pub fn new(local: TransformData) -> Self {
        use cgmath::Zero;
        Transform {
            local,
            global: cgmath::Matrix4::zero(),
            needs_update: true,
            changed: true,
        }
    }

    pub fn get_local(self) -> TransformData {
        self.local
    }
    pub fn set_local(&mut self, transform_data: TransformData) {
        self.local = transform_data;
        self.needs_update = true;
    }

    pub fn local_matrix(self) -> cgmath::Matrix4<f32> {
        cgmath::Matrix4::from_translation(self.local.position)
            * cgmath::Matrix4::from(self.local.rotation)
            * cgmath::Matrix4::from_nonuniform_scale(
                self.local.scale.x,
                self.local.scale.y,
                self.local.scale.z,
            )
    }

    pub(crate) fn to_instance_raw(self) -> crate::InstanceRaw {
        use cgmath::{Matrix, SquareMatrix};

        let model = self.global;
        let normal = model
            .invert()
            .unwrap_or_else(cgmath::Matrix4::identity)
            .transpose();
        let normal = cgmath::Matrix3::from_cols(
            normal.x.truncate(),
            normal.y.truncate(),
            normal.z.truncate(),
        );

        crate::InstanceRaw {
            model: model.into(),
            normal: normal.into(),
        }
    }
}

// TODO: if this ever is more than a toy renderer then really proper ECS is required lol
#[derive(Debug)]
pub struct Entity {
    pub mesh: Option<crate::model::MeshHandle>,
    pub transform: Transform,
    children: Vec<EntityHandle>,
    parent: Option<EntityHandle>,
}

impl Entity {
    pub fn new(mesh: Option<crate::model::MeshHandle>, transform: TransformData) -> Self {
        Self {
            mesh,
            transform: Transform::new(transform),
            children: vec![],
            parent: None,
        }
    }
}

pub struct EntityManager {
    entities: RwLock<EntityList>,
}
crate::manager::manager!(EntityManager, EntityList, entities, EntityHandle, Entity);

impl EntityList {
    pub fn make_parent(&mut self, parent: EntityHandle, child: EntityHandle) {
        let child_ref = self.get_mut(child);
        assert!(child_ref.parent.is_none());
        child_ref.parent = Some(parent);

        let parent_ref = self.get_mut(parent);
        parent_ref.children.push(child);
    }
}
impl EntityManager {
    pub fn new() -> Self {
        Self {
            entities: RwLock::new(EntityList(vec![])),
        }
    }

    pub fn make_parent(&self, parent: EntityHandle, child: EntityHandle) {
        let mut entities = self.entities.write();
        entities.make_parent(parent, child);
    }
}

pub fn propagate_transforms(entity_manager: &EntityManager) {
    use cgmath::SquareMatrix;

    fn recurse(
        entity_list: &mut EntityList,
        entity: EntityHandle,
        parent_transform: cgmath::Matrix4<f32>,
        needs_update: bool,
    ) {
        let (children_needs_update, children_parent_transform) = {
            let e = entity_list.get_mut(entity);
            let needs_update = needs_update | e.transform.needs_update;
            if needs_update {
                let mat = e.transform.local_matrix();
                e.transform.global = parent_transform * mat;
                e.transform.needs_update = false;
                e.transform.changed = true;
            }
            (needs_update, e.transform.global)
        };
        let children = entity_list.get(entity).children.clone();
        for c in children {
            recurse(
                entity_list,
                c,
                children_parent_transform,
                children_needs_update,
            );
        }
    }

    let mut entity_list = entity_manager.get_all_mut();

    let roots = entity_list
        .get_all_handles()
        .filter(|e| entity_list.get(*e).parent.is_none())
        .collect_vec();
    for r in roots {
        recurse(&mut entity_list, r, cgmath::Matrix4::identity(), false);
    }
}

pub fn collect_instances(
    entity_manager: &EntityManager,
    meshes: &crate::model::MeshManager,
    device_queue: &crate::DeviceQueue,
) {
    let mut map: HashMap<MeshHandle, (bool, Vec<_>)> = HashMap::new();
    for e in entity_manager.get_all_mut().iter_mut() {
        let mesh = if let Some(m) = e.mesh {
            m
        } else {
            continue;
        };
        let entry = map.entry(mesh).or_insert_with(|| (true, vec![]));
        entry.0 |= e.transform.changed;
        e.transform.changed = false;
        entry.1.push(e.transform.to_instance_raw());
    }
    let mut list = meshes.get_all_mut();
    let (list, handles) = list.get_all_handles_mut();
    for m in handles {
        let entry = map.get(&m);
        if let Some(entry) = entry {
            if entry.0 {
                let mut mesh = list.get_mut(m);
                let instances_prev = mesh.instances.take();
                let needs_new_buffer = instances_prev
                    .as_ref()
                    .map_or(true, |i| i.num_instances != entry.1.len() as u32);
                let buffer = if needs_new_buffer {
                    device_queue.device.create_buffer(&wgpu::BufferDescriptor {
                        label: Some(&format!("{} instance buffer", mesh.name)),
                        size: (std::mem::size_of::<crate::InstanceRaw>() * entry.1.len()) as u64,
                        usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
                        mapped_at_creation: false,
                    })
                } else {
                    instances_prev.unwrap().buffer
                };
                device_queue
                    .queue
                    .write_buffer(&buffer, 0, bytemuck::cast_slice(&entry.1));
                mesh.instances = Some(crate::model::MeshInstances {
                    buffer,
                    num_instances: entry.1.len() as u32,
                });
            }
        } else {
            list.get_mut(m).instances = None;
        }
    }
}
