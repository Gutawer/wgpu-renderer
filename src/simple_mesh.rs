use wgpu::util::DeviceExt;

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct SimpleVertex {
    position: [f32; 3],
}

impl crate::model::Vertex for SimpleVertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        const ATTRS: [wgpu::VertexAttribute; 1] = wgpu::vertex_attr_array![
            0 => Float32x3
        ];
        use std::mem;

        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &ATTRS,
        }
    }
}

pub struct SimpleMesh {
    vertex_buffer: wgpu::Buffer,
    indices: Vec<u32>,
    index_buffer: wgpu::Buffer,
}

impl SimpleMesh {
    pub fn from_vertices_indices(
        device_queue: crate::DeviceQueue,
        label: &str,
        vertices: Vec<[f32; 3]>,
        indices: Vec<u32>,
    ) -> Self {
        let vertices: Vec<SimpleVertex> = bytemuck::cast_vec(vertices);
        let vertex_buffer =
            device_queue
                .device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("{:?} Vertex Buffer", label)),
                    contents: bytemuck::cast_slice(&vertices),
                    usage: wgpu::BufferUsages::VERTEX,
                });
        let index_buffer =
            device_queue
                .device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("{:?} Index Buffer", label)),
                    contents: bytemuck::cast_slice(&indices),
                    usage: wgpu::BufferUsages::INDEX,
                });
        Self {
            vertex_buffer,
            indices,
            index_buffer,
        }
    }
}

pub trait DrawCubemapMesh<'a, 'b>
where
    'b: 'a,
{
    fn draw_cubemap_mesh(&mut self, mesh: &'b SimpleMesh, instance_num: u32);
}

impl<'a, 'b, T: wgpu::util::RenderEncoder<'a>> DrawCubemapMesh<'a, 'b> for T
where
    'b: 'a,
{
    fn draw_cubemap_mesh(&mut self, mesh: &'b SimpleMesh, instance_num: u32) {
        self.set_vertex_buffer(0, mesh.vertex_buffer.slice(..));
        self.set_index_buffer(mesh.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
        self.draw_indexed(
            0..mesh.indices.len() as u32,
            0,
            instance_num..instance_num + 1,
        );
    }
}

pub fn get_cube_mesh(device_queue: crate::DeviceQueue) -> SimpleMesh {
    let vertices = vec![
        [1.0, 1.0, -1.0],
        [1.0, -1.0, -1.0],
        [1.0, 1.0, 1.0],
        [1.0, -1.0, 1.0],
        [-1.0, 1.0, -1.0],
        [-1.0, -1.0, -1.0],
        [-1.0, 1.0, 1.0],
        [-1.0, -1.0, 1.0],
    ];
    let indices = vec![
        4, 0, 2, 2, 3, 7, 6, 7, 5, 1, 5, 7, 0, 1, 3, 4, 5, 1, 4, 2, 6, 2, 7, 6, 6, 5, 4, 1, 7, 3,
        0, 3, 2, 4, 1, 0,
    ];
    SimpleMesh::from_vertices_indices(device_queue, "Cube", vertices, indices)
}
