#[allow(unused_macros)]
macro_rules! manager {
    ($s: ident, $list: ident, $list_field: ident, $handle: ident, $item: ty) => {
        #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
        pub struct $handle(usize);

        #[derive(Debug)]
        pub struct $list(Vec<$item>);
        impl $list {
            #[allow(unused)]
            pub fn get<'a>(&'a self, handle: $handle) -> &'a $item {
                &self.0[handle.0]
            }

            #[allow(unused)]
            pub fn get_mut<'a>(&'a mut self, handle: $handle) -> &'a mut $item {
                &mut self.0[handle.0]
            }

            #[allow(unused)]
            pub fn get_all_handles<'a>(&'a self) -> impl Iterator<Item = $handle> + 'a {
                (0..self.0.len()).map($handle)
            }
            #[allow(unused)]
            pub fn get_all_handles_mut<'a>(
                &'a mut self,
            ) -> (&'a mut Self, impl Iterator<Item = $handle> + 'a) {
                let len = self.0.len();
                (self, (0..len).map($handle))
            }

            #[allow(unused)]
            pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a $item> {
                self.0.iter()
            }
            #[allow(unused)]
            pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = &'a mut $item> {
                self.0.iter_mut()
            }
        }

        impl $s {
            #[allow(unused)]
            pub fn get<'a>(
                &'a self,
                handle: $handle,
            ) -> impl core::ops::Deref<Target = $item> + 'a {
                let list = self.$list_field.read();
                parking_lot::RwLockReadGuard::map(list, |t| t.get(handle))
            }

            #[allow(unused)]
            pub fn get_mut<'a>(
                &'a self,
                handle: $handle,
            ) -> impl core::ops::DerefMut<Target = $item> + 'a {
                let list = self.$list_field.write();
                parking_lot::RwLockWriteGuard::map(list, |t| t.get_mut(handle))
            }

            #[allow(unused)]
            pub fn get_all<'a>(&'a self) -> impl core::ops::Deref<Target = $list> + 'a {
                self.$list_field.read()
            }
            #[allow(unused)]
            pub fn get_all_mut<'a>(&'a self) -> impl core::ops::DerefMut<Target = $list> + 'a {
                self.$list_field.write()
            }

            #[allow(unused)]
            pub fn add(&self, item: $item) -> $handle {
                let mut list = self.$list_field.write();
                list.0.push(item);
                $handle(list.0.len() - 1)
            }
        }
    };
}
pub(crate) use manager;
