#[derive(Debug)]
pub struct FullScreenDrawer {
    pipeline: wgpu::RenderPipeline,
}

impl FullScreenDrawer {
    pub fn new(
        device_queue: crate::DeviceQueue,
        shaders: &crate::shaders::Shaders,
        (module, entry): (&wgpu::ShaderModule, &str),
        layout: &wgpu::PipelineLayout,
        color_formats: &[wgpu::TextureFormat],
        depth: Option<(wgpu::TextureFormat, wgpu::CompareFunction)>,
    ) -> Self {
        let pipeline =
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Full Screen Draw Pipeline",
                device_queue,
                layout,
                color_formats,
                depth,
                cull_mode: None,
                vertex_layouts: &[],
                vert: (&shaders.full_screen_vert, "main"),
                frag: (module, entry),
            });
        Self { pipeline }
    }

    pub fn set_pipeline<'a, 'b: 'a>(&'b self, render_pass: &mut wgpu::RenderPass<'a>) {
        render_pass.set_pipeline(&self.pipeline);
    }

    pub fn render<'a, 'b: 'a>(&'b self, render_pass: &mut wgpu::RenderPass<'a>) {
        render_pass.draw(0..3, 0..1);
    }
}
