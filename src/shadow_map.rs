use core::num::NonZeroU64;
use std::convert::TryInto;
use std::sync::Arc;

use itertools::Itertools;
use wgpu_profiler::*;

use crate::light::PointLight;
use crate::model::{DrawModelRaw, Vertex};

const LIGHT_SIZE: usize = std::mem::size_of::<crate::light::GPUPointLight>();

#[derive(Clone, Copy, PartialEq, Eq, Debug, Default)]
pub struct Location {
    pub position: (u32, u32),
    pub size: u32,
}

const fn num_bits<T>() -> usize {
    std::mem::size_of::<T>() * 8
}
fn log2(x: u16) -> u8 {
    assert!(x > 0);
    (num_bits::<u16>() as u32 - x.leading_zeros() - 1)
        .try_into()
        .unwrap()
}

#[cfg(all(
    any(target_arch = "x86", target_arch = "x86_64"),
    target_feature = "bmi2"
))]
fn squash(bits: u32) -> u16 {
    use bitintr::Pext;
    bits.pext(0x55555555) as u16
}
#[cfg(not(all(
    any(target_arch = "x86", target_arch = "x86_64"),
    target_feature = "bmi2"
)))]
#[allow(clippy::identity_op)]
fn squash(bits: u32) -> u16 {
    let mut x = bits;
    x = ((x & 0x44444444) >> 1) | ((x & 0x11111111) >> 0);
    x = ((x & 0x30303030) >> 2) | ((x & 0x03030303) >> 0);
    x = ((x & 0x0F000F00) >> 4) | ((x & 0x000F000F) >> 0);
    x = ((x & 0x00FF0000) >> 8) | ((x & 0x000000FF) >> 0);

    x as u16
}

fn can_fit(raw: u32, to_add: u32, max: u32) -> u32 {
    (max - raw) / to_add
}

enum LightAllocation {
    Point {
        index: usize,
        positions: [[u32; 2]; 6],
        size: u32,
    },
    #[allow(dead_code)]
    Spot { index: usize, location: Location },
}

fn allocate<'a, I, J>(
    base_size_log: u8,
    allocator_lights: I,
) -> impl Iterator<Item = LightAllocation> + 'a
where
    I: IntoIterator<Item = J> + 'a,
    J: IntoIterator<Item = AllocatorLight> + 'a,
{
    let max = 1 << (base_size_log << 1);
    let allocator_lights = allocator_lights
        .into_iter()
        .take((base_size_log + 1).into());

    let lights_flat = allocator_lights
        .enumerate()
        .flat_map(|(i, l)| l.into_iter().map(move |x| (i, x)));

    // this algorithm is sorta weird and based on binary counting
    // after writing a more obvious tree-based version i was writing the unit tests
    // and realised that the pattern x and y coordinates followed was very much like a weird
    // form of binary counting
    //
    // notably the x value is the odd bits of the number (here called `raw`) and the y value
    // is the even bits
    //
    // to get the next location the currently used `size` (squared) is added to `raw` and then the bits
    // are extracted - the bit extraction is somewhat difficult but modern x86 has an instruction for
    // doing this quickly (otherwise a still-probably-fast implementation via bitwise ops is used)
    lights_flat
        .scan(0, move |raw, (level, l)| {
            let shift = base_size_log - level as u8;
            let size = 1 << shift;
            let to_add = 1 << (shift << 1);
            let remaining = can_fit(*raw, to_add, max) as usize;
            match l {
                AllocatorLight::Point { index } => {
                    if remaining == 0 {
                        return None;
                    }
                    if remaining < 6 {
                        return Some(None);
                    }
                    let positions = [0, 1, 2, 3, 4, 5].map(|_| {
                        let x = squash(*raw) as u32;
                        let y = squash(*raw >> 1) as u32;
                        *raw += to_add;
                        [x, y]
                    });
                    Some(Some(LightAllocation::Point {
                        index,
                        positions,
                        size,
                    }))
                }
                AllocatorLight::Spot { index } => {
                    if remaining == 0 {
                        return None;
                    }
                    let location = {
                        let x = squash(*raw) as u32;
                        let y = squash(*raw >> 1) as u32;
                        *raw += to_add;
                        Location {
                            position: (x, y),
                            size,
                        }
                    };
                    Some(Some(LightAllocation::Spot { index, location }))
                }
            }
        })
        .flatten()
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AllocatorLight {
    Point {
        index: usize,
    },
    #[allow(dead_code)]
    Spot {
        index: usize,
    },
}

#[repr(C)]
#[derive(Debug, PartialEq, Eq, Clone, Copy, bytemuck::Pod, bytemuck::Zeroable)]
pub struct AllocatedPointLight {
    positions: [[u32; 2]; 6],
    size: u32,
    _padding: u32,
}

#[derive(Debug, PartialEq, Eq)]
pub struct AllocatedLights {
    point_light_allocations: Vec<AllocatedPointLight>,
}

fn allocate_from_grouped_lights<I, J>(
    base_size_log: u8,
    groups: I,
    point_lights: &[PointLight],
    allocated_lights: &mut AllocatedLights,
) where
    I: IntoIterator<Item = J>,
    J: IntoIterator<Item = AllocatorLight>,
{
    let allocations = allocate(base_size_log, groups);
    allocated_lights.point_light_allocations.resize(
        point_lights.len(),
        AllocatedPointLight {
            positions: [[0, 0]; 6],
            size: 0,
            _padding: 0,
        },
    );
    for a in allocated_lights.point_light_allocations.iter_mut() {
        a.size = 0;
    }
    for a in allocations {
        match a {
            LightAllocation::Point {
                index,
                positions,
                size,
            } => {
                let p = &mut allocated_lights.point_light_allocations[index];
                p.positions = positions;
                p.size = size;
            }
            LightAllocation::Spot { .. } => unimplemented!(),
        }
    }
}

fn get_point_light_bounds(
    light: &PointLight,
    camera: &crate::camera::Camera,
) -> Option<[cgmath::Point2<f32>; 2]> {
    use cgmath::{point2, point3, vec3};

    let (cx, cy, cz) = light.position.into();
    let r = light.radius;
    let delta = camera.pos - light.position;
    let abs_delta = vec3(delta.x.abs(), delta.y.abs(), delta.z.abs());
    if abs_delta.x <= r && abs_delta.y <= r && abs_delta.z <= r {
        return Some([point2(-1.0, -1.0), point2(1.0, 1.0)]);
    }

    let project = |x, y, z| {
        let p = point3(x, y, z);
        camera.view_proj * p.to_homogeneous()
    };
    let corners = [
        project(cx + r, cy + r, cz + r),
        project(cx + r, cy + r, cz - r),
        project(cx + r, cy - r, cz + r),
        project(cx + r, cy - r, cz - r),
        project(cx - r, cy + r, cz + r),
        project(cx - r, cy + r, cz - r),
        project(cx - r, cy - r, cz + r),
        project(cx - r, cy - r, cz - r),
    ];
    let corners_filtered = corners
        .iter()
        .filter(|v| 0.0 <= v.z && v.z <= v.w)
        .map(|v| cgmath::Point3::from_homogeneous(*v));

    let oob = |min, max, range: core::ops::Range<f32>| {
        (min < range.start && max < range.start) || (min > range.end && max > range.end)
    };
    let (min_x, max_x) = corners_filtered
        .clone()
        .map(|p| p.x)
        .minmax()
        .into_option()?;
    let (min_y, max_y) = corners_filtered
        .clone()
        .map(|p| p.y)
        .minmax()
        .into_option()?;
    if oob(min_x, max_x, -1.0..1.0) {
        return None;
    }
    if oob(min_y, max_y, -1.0..1.0) {
        return None;
    }

    let (min_x, max_x) = (f32::max(min_x, -1.0), f32::min(max_x, 1.0));
    let (min_y, max_y) = (f32::max(min_y, -1.0), f32::min(max_y, 1.0));

    Some([point2(min_x, min_y), point2(max_x, max_y)])
}

fn ndc_to_screen(
    [min, max]: [cgmath::Point2<f32>; 2],
    (size_x, size_y): (u32, u32),
) -> [cgmath::Point2<u32>; 2] {
    let min_screen = cgmath::Point2::new(
        ((min.x + 1.0) / 2.0 * size_x as f32).floor() as u32,
        ((min.y + 1.0) / 2.0 * size_y as f32).floor() as u32,
    );
    let max_screen = cgmath::Point2::new(
        ((max.x + 1.0) / 2.0 * size_x as f32).ceil() as u32,
        ((max.y + 1.0) / 2.0 * size_y as f32).ceil() as u32,
    );
    [min_screen, max_screen]
}

fn screen_bounds_to_depth_level([min, max]: [cgmath::Point2<u32>; 2], base_size_log: u8) -> u8 {
    let size_x = max.x - min.x;
    let size_y = max.y - min.y;
    let size = u32::max(size_x, size_y);
    let size = u32::min(size, 0x8000) as u16;
    let size = size.next_power_of_two();
    let size = u16::min(size, 1024);
    let l = log2(size);
    if l > base_size_log {
        return base_size_log;
    }
    base_size_log - l
}

fn light_to_depth_level(
    light: &PointLight,
    camera: &crate::camera::Camera,
    screen_size: (u32, u32),
    base_size_log: u8,
) -> Option<u8> {
    let bounds = get_point_light_bounds(light, camera)?;
    let screen_bounds = ndc_to_screen(bounds, screen_size);
    let depth_level = screen_bounds_to_depth_level(screen_bounds, base_size_log);

    Some(depth_level)
}

fn groups_from_lights(
    point_lights: &[PointLight],
    camera: &crate::camera::Camera,
    screen_size: (u32, u32),
    base_size_log: u8,
    groups: &mut Vec<Vec<AllocatorLight>>,
) {
    groups.resize(base_size_log as usize + 1, vec![]);
    for g in groups.iter_mut() {
        g.clear();
    }
    for (i, l) in point_lights.iter().enumerate() {
        let level = light_to_depth_level(l, camera, screen_size, base_size_log);
        let level = if let Some(l) = level {
            l
        } else {
            continue;
        };
        let a = AllocatorLight::Point { index: i };
        groups[level as usize].push(a);
    }
}

pub struct ShadowMapManager {
    groups: Vec<Vec<AllocatorLight>>,
    allocated_lights: AllocatedLights,
    pub shadow_map_texture: crate::texture::DepthTexture,
    size: u16,
    capture_view_projections_buffer: Arc<wgpu::Buffer>,
    proj_padded_size: u32,

    shadow_map_renderer_pipeline: wgpu::RenderPipeline,
    shadow_map_renderer_bind_group_layout: wgpu::BindGroupLayout,
    shadow_map_renderer_bind_group: wgpu::BindGroup,

    working_light_buffer: wgpu::Buffer,

    pub shadow_map_point_light_location_buffer: wgpu::Buffer,
    clearer: crate::full_screen_draw::FullScreenDrawer,
}

impl ShadowMapManager {
    pub fn new(
        device_queue: crate::DeviceQueue,
        shaders: &crate::shaders::Shaders,
        size: u16,
        capture_view_projections_buffer: Arc<wgpu::Buffer>,
        proj_padded_size: u32,
        num_lights: usize,
    ) -> Self {
        let groups = vec![];
        let allocated_lights = AllocatedLights {
            point_light_allocations: vec![],
        };
        let shadow_map_texture = crate::texture::DepthTexture::create_depth_texture(
            device_queue,
            size as u32,
            size as u32,
            "Shadow Map Atlas",
            wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        );
        let shadow_map_renderer_bind_group_layout =
            device_queue
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("Shadow Map Renderer Bind Group Layout"),
                    entries: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::VERTEX,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: true,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                    ],
                });
        let shadow_map_renderer_pipeline = {
            let layout =
                device_queue
                    .device
                    .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                        label: Some("Shadow Map Renderer Pipeline Layout"),
                        bind_group_layouts: &[&shadow_map_renderer_bind_group_layout],
                        push_constant_ranges: &[],
                    });
            crate::pipeline::create_render_pipeline(crate::pipeline::RenderPipelineConfig {
                label: "Shadow Map Renderer Pipeline",
                device_queue,
                layout: &layout,
                color_formats: &[],
                depth: Some((
                    crate::texture::DepthTexture::DEPTH_FORMAT,
                    wgpu::CompareFunction::LessEqual,
                )),
                cull_mode: Some(wgpu::Face::Front),
                vertex_layouts: &[
                    crate::model::RawModelVertex::desc(),
                    crate::InstanceRaw::desc(),
                ],
                vert: (&shaders.shadow, "main"),
                frag: (&shaders.shadow, "main"),
            })
        };
        let working_light_buffer = device_queue.device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Working Light Buffer"),
            size: LIGHT_SIZE as u64,
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::UNIFORM,
            mapped_at_creation: false,
        });
        let shadow_map_renderer_bind_group =
            device_queue
                .device
                .create_bind_group(&wgpu::BindGroupDescriptor {
                    label: Some("Shadow Map Renderer Bind Group"),
                    layout: &shadow_map_renderer_bind_group_layout,
                    entries: &[
                        wgpu::BindGroupEntry {
                            binding: 0,
                            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                                buffer: &capture_view_projections_buffer,
                                offset: 0,
                                size: Some(NonZeroU64::new(64).unwrap() as wgpu::BufferSize),
                            }),
                        },
                        wgpu::BindGroupEntry {
                            binding: 1,
                            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                                buffer: &working_light_buffer,
                                offset: 0,
                                size: Some(NonZeroU64::new(40).unwrap() as wgpu::BufferSize),
                            }),
                        },
                    ],
                });
        let shadow_map_point_light_location_buffer =
            device_queue.device.create_buffer(&wgpu::BufferDescriptor {
                label: Some("Shadow Map Light Location Buffer"),
                size: (num_lights * std::mem::size_of::<AllocatedPointLight>())
                    as wgpu::BufferAddress,
                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::STORAGE,
                mapped_at_creation: false,
            });
        let clearer = {
            let layout =
                device_queue
                    .device
                    .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                        label: Some("Shadow Map Clearer Pipeline Layout"),
                        bind_group_layouts: &[],
                        push_constant_ranges: &[],
                    });
            crate::full_screen_draw::FullScreenDrawer::new(
                device_queue,
                shaders,
                (&shaders.shadow, "clear"),
                &layout,
                &[],
                Some((
                    crate::texture::DepthTexture::DEPTH_FORMAT,
                    wgpu::CompareFunction::Always,
                )),
            )
        };
        Self {
            groups,
            allocated_lights,
            shadow_map_texture,
            size,
            capture_view_projections_buffer,
            proj_padded_size,
            shadow_map_renderer_pipeline,
            shadow_map_renderer_bind_group_layout,
            shadow_map_renderer_bind_group,
            shadow_map_point_light_location_buffer,
            clearer,
            working_light_buffer,
        }
    }

    #[allow(unused)]
    pub fn light_buffer_change(
        &mut self,
        device_queue: crate::DeviceQueue,
        light_buffer: &wgpu::Buffer,
        new_num_lights: Option<usize>,
    ) {
        self.shadow_map_renderer_bind_group =
            device_queue
                .device
                .create_bind_group(&wgpu::BindGroupDescriptor {
                    label: Some("Shadow Map Renderer Bind Group"),
                    layout: &self.shadow_map_renderer_bind_group_layout,
                    entries: &[
                        wgpu::BindGroupEntry {
                            binding: 0,
                            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                                buffer: &self.capture_view_projections_buffer,
                                offset: 0,
                                size: Some(
                                    NonZeroU64::new(std::mem::size_of::<[[f32; 4]; 4]>() as u64)
                                        .unwrap()
                                        as wgpu::BufferSize,
                                ),
                            }),
                        },
                        wgpu::BindGroupEntry {
                            binding: 1,
                            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                                buffer: light_buffer,
                                offset: 0,
                                size: Some(
                                    NonZeroU64::new(LIGHT_SIZE as u64).unwrap() as wgpu::BufferSize
                                ),
                            }),
                        },
                    ],
                });
        if let Some(n) = new_num_lights {
            self.shadow_map_point_light_location_buffer =
                device_queue.device.create_buffer(&wgpu::BufferDescriptor {
                    label: Some("Shadow Map Light Location Buffer"),
                    size: (n * std::mem::size_of::<AllocatedPointLight>()) as wgpu::BufferAddress,
                    usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::STORAGE,
                    mapped_at_creation: false,
                });
        }
    }

    pub fn calculate_light_shadow_map_locations(
        &mut self,
        point_lights: &[PointLight],
        camera: &crate::camera::Camera,
        screen_size: (u32, u32),
        device_queue: crate::DeviceQueue,
    ) {
        let base_size_log = log2(self.size);
        groups_from_lights(
            point_lights,
            camera,
            screen_size,
            base_size_log,
            &mut self.groups,
        );
        allocate_from_grouped_lights(
            base_size_log,
            self.groups.iter().map(|x| x.iter().copied()),
            point_lights,
            &mut self.allocated_lights,
        );
        device_queue.queue.write_buffer(
            &self.shadow_map_point_light_location_buffer,
            0,
            bytemuck::cast_slice(&self.allocated_lights.point_light_allocations),
        );
    }

    pub fn render_shadow_maps<'a, 'b: 'a>(
        &'b self,
        device_queue: crate::DeviceQueue,
        encoder: &'a mut wgpu::CommandEncoder,
        instance_groups: &[(&'a crate::model::Mesh, &'a wgpu::Buffer, u32)],
        lights: &[PointLight],
        light_buffer: &wgpu::Buffer,
    ) {
        for (li, (_, p)) in lights
            .iter()
            .zip(&self.allocated_lights.point_light_allocations)
            .enumerate()
        {
            encoder.copy_buffer_to_buffer(
                light_buffer,
                li as u64 * LIGHT_SIZE as u64,
                &self.working_light_buffer,
                0,
                LIGHT_SIZE as u64,
            );
            let render_pass_label = format!("Shadow Map Render Pass {}", li);
            let render_pass = &mut encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some(&render_pass_label),
                color_attachments: &[],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                    view: &self.shadow_map_texture.view,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });
            if p.size == 0 {
                continue;
            }
            wgpu_profiler!(
                &format!("light clear {}", li),
                device_queue.profiler.lock(),
                render_pass,
                device_queue.device,
                {
                    self.clearer.set_pipeline(render_pass);
                    for (face, [x, y]) in (0..6).zip(p.positions) {
                        wgpu_profiler!(
                            &format!("face {}", face),
                            device_queue.profiler.lock(),
                            render_pass,
                            device_queue.device,
                            {
                                render_pass.set_viewport(
                                    x as f32,
                                    y as f32,
                                    p.size as f32,
                                    p.size as f32,
                                    0.0,
                                    1.0,
                                );
                                render_pass.set_scissor_rect(x, y, p.size, p.size);
                                self.clearer.render(render_pass);
                            }
                        );
                    }
                }
            );
            wgpu_profiler!(
                &format!("light {}", li),
                device_queue.profiler.lock(),
                render_pass,
                device_queue.device,
                {
                    render_pass.set_pipeline(&self.shadow_map_renderer_pipeline);
                    for (face, [x, y]) in (0..6).zip(p.positions) {
                        wgpu_profiler!(
                            &format!("face {}", face),
                            device_queue.profiler.lock(),
                            render_pass,
                            device_queue.device,
                            {
                                render_pass.set_bind_group(
                                    0,
                                    &self.shadow_map_renderer_bind_group,
                                    &[(face * self.proj_padded_size) as wgpu::DynamicOffset],
                                );
                                render_pass.set_viewport(
                                    x as f32,
                                    y as f32,
                                    p.size as f32,
                                    p.size as f32,
                                    0.0,
                                    1.0,
                                );
                                render_pass.set_scissor_rect(x, y, p.size, p.size);
                                for (ii, (m, b, i)) in instance_groups.iter().enumerate() {
                                    wgpu_profiler!(
                                        &format!("model {}", ii),
                                        device_queue.profiler.lock(),
                                        render_pass,
                                        device_queue.device,
                                        {
                                            render_pass.set_vertex_buffer(1, b.slice(..));
                                            render_pass.draw_mesh_instanced_raw(m, 0..*i);
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    fn r(num: usize) -> impl Iterator<Item = AllocatorLight> {
        std::iter::repeat(AllocatorLight::Spot { index: 0 }).take(num)
    }

    fn allocate_test(size: u16, counts: &[usize]) -> Vec<Location> {
        super::allocate(log2(size), counts.iter().map(|&x| r(x)))
            .map(|a| match a {
                LightAllocation::Spot { location, .. } => location,
                _ => panic!(),
            })
            .collect()
    }

    #[test]
    fn test_atlas_allocator() {
        let allocated = allocate_test(16, &[0, 0, 0, 0, 1]);
        assert_eq!(
            allocated,
            vec![Location {
                position: (0, 0),
                size: 1
            }]
        );

        let allocated = allocate_test(16, &[0, 0, 0, 0, 0, 1]);
        assert_eq!(allocated, vec![]);

        let allocated = allocate_test(1024, &[1]);
        assert_eq!(
            allocated,
            vec![Location {
                position: (0, 0),
                size: 1024
            }]
        );

        let allocated = allocate_test(1024, &[0, 2]);
        assert_eq!(
            allocated,
            vec![
                Location {
                    position: (0, 0),
                    size: 512
                },
                Location {
                    position: (512, 0),
                    size: 512
                },
            ]
        );

        let allocated = allocate_test(1024, &[0, 4, 2]);
        assert_eq!(
            allocated,
            vec![
                Location {
                    position: (0, 0),
                    size: 512
                },
                Location {
                    position: (512, 0),
                    size: 512
                },
                Location {
                    position: (0, 512),
                    size: 512
                },
                Location {
                    position: (512, 512),
                    size: 512
                },
            ]
        );

        let allocated = allocate_test(1024, &[0, 5, 2]);
        assert_eq!(
            allocated,
            vec![
                Location {
                    position: (0, 0),
                    size: 512
                },
                Location {
                    position: (512, 0),
                    size: 512
                },
                Location {
                    position: (0, 512),
                    size: 512
                },
                Location {
                    position: (512, 512),
                    size: 512
                },
            ]
        );

        let allocated = allocate_test(1024, &[0, 2, 5]);
        assert_eq!(
            allocated,
            vec![
                Location {
                    position: (0, 0),
                    size: 512
                },
                Location {
                    position: (512, 0),
                    size: 512
                },
                Location {
                    position: (0, 512),
                    size: 256
                },
                Location {
                    position: (256, 512),
                    size: 256
                },
                Location {
                    position: (0, 512 + 256),
                    size: 256
                },
                Location {
                    position: (256, 512 + 256),
                    size: 256
                },
                Location {
                    position: (512, 512),
                    size: 256
                },
            ]
        );
    }

    #[test]
    #[allow(clippy::identity_op)]
    fn test_atlas_allocator_groups() {
        let point_lights = vec![
            PointLight {
                position: cgmath::Point3::new(0.0, 0.0, 0.0),
                color: Default::default(),
                radius: Default::default(),
                luminous_flux: Default::default(),
                enabled: false
            };
            5
        ];
        let p = |index| AllocatorLight::Point { index };
        let lights: &[&[AllocatorLight]] = &[&[], &[], &[], &[p(0), p(1), p(2)], &[p(3), p(4)]];
        let mut allocated = AllocatedLights {
            point_light_allocations: vec![],
        };
        allocate_from_grouped_lights(
            log2(2048),
            lights.iter().map(|&x| x.iter().copied()),
            &point_lights,
            &mut allocated,
        );
        #[rustfmt::skip]
        assert_eq!(
            allocated,
            AllocatedLights {
                point_light_allocations: vec![
                    AllocatedPointLight {
                        size: 256,
                        positions: [
                            [   0 +   0 +   0 +   0,    0 +   0 +   0 +   0],
                            [   0 +   0 + 256 +   0,    0 +   0 +   0 +   0],
                            [   0 +   0 +   0 +   0,    0 +   0 + 256 +   0],
                            [   0 +   0 + 256 +   0,    0 +   0 + 256 +   0],
                            [   0 + 512 +   0 +   0,    0 +   0 +   0 +   0],
                            [   0 + 512 + 256 +   0,    0 +   0 +   0 +   0],
                        ],
                        _padding: 0
                    },
                    AllocatedPointLight {
                        size: 256,
                        positions: [
                            [   0 + 512 +   0 +   0,    0 +   0 + 256 +   0],
                            [   0 + 512 + 256 +   0,    0 +   0 + 256 +   0],
                            [   0 +   0 +   0 +   0,    0 + 512 +   0 +   0],
                            [   0 +   0 + 256 +   0,    0 + 512 +   0 +   0],
                            [   0 +   0 +   0 +   0,    0 + 512 + 256 +   0],
                            [   0 +   0 + 256 +   0,    0 + 512 + 256 +   0],
                        ],
                        _padding: 0
                    },
                    AllocatedPointLight {
                        size: 256,
                        positions: [
                            [   0 + 512 +   0 +   0,    0 + 512 +   0 +   0],
                            [   0 + 512 + 256 +   0,    0 + 512 +   0 +   0],
                            [   0 + 512 +   0 +   0,    0 + 512 + 256 +   0],
                            [   0 + 512 + 256 +   0,    0 + 512 + 256 +   0],
                            [1024 +   0 +   0 +   0,    0 +   0 +   0 +   0],
                            [1024 +   0 + 256 +   0,    0 +   0 +   0 +   0],
                        ],
                        _padding: 0
                    },
                    AllocatedPointLight {
                        size: 128,
                        positions: [
                            [1024 +   0 +   0 +   0,    0 +   0 + 256 +   0],
                            [1024 +   0 +   0 + 128,    0 +   0 + 256 +   0],
                            [1024 +   0 +   0 +   0,    0 +   0 + 256 + 128],
                            [1024 +   0 +   0 + 128,    0 +   0 + 256 + 128],
                            [1024 +   0 + 256 +   0,    0 +   0 + 256 +   0],
                            [1024 +   0 + 256 + 128,    0 +   0 + 256 +   0],
                        ],
                        _padding: 0
                    },
                    AllocatedPointLight {
                        size: 128,
                        positions: [
                            [1024 +   0 + 256 +   0,    0 +   0 + 256 + 128],
                            [1024 +   0 + 256 + 128,    0 +   0 + 256 + 128],
                            [1024 + 512 +   0 +   0,    0 +   0 +   0 +   0],
                            [1024 + 512 +   0 + 128,    0 +   0 +   0 +   0],
                            [1024 + 512 +   0 +   0,    0 +   0 +   0 + 128],
                            [1024 + 512 +   0 + 128,    0 +   0 +   0 + 128],
                        ],
                        _padding: 0
                    }
                ]
            }
        );
    }
}
