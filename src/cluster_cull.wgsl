struct ClusterAabb {
	[[align(16)]]
	min_point: vec3<f32>;
	[[align(16)]]
	max_point: vec3<f32>;
};
[[block]] struct Clusters {
	arr: array<ClusterAabb>;
};

[[block]]
struct Uniforms {
	inv_proj: mat4x4<f32>;
	view: mat4x4<f32>;
	proj: mat4x4<f32>;
	view_proj: mat4x4<f32>;
	view_pos: vec4<f32>;
	screen_dimensions: vec2<u32>;

	tile_sizes: vec2<f32>;

	z_near: f32;
	z_far: f32;
};

struct Light {
	position: vec3<f32>;
	color: vec3<f32>;
	radius: f32;
	intensity: f32;
	enabled: u32;
};
[[block]] struct Lights {
	arr: array<Light>;
};

[[block]] struct LightIndices {
	count: atomic<u32>;
	arr: array<u32>;
};

struct LightGridEntry {
	offset: u32;
	count: u32;
};
[[block]] struct LightGrid {
	arr: array<LightGridEntry>;
};

[[group(0), binding(0)]]
var<uniform> uniforms: Uniforms;
[[group(0), binding(1)]]
var<storage, read_write> clusters: Clusters;
[[group(0), binding(2)]]
var<storage, read> lights: Lights;
[[group(0), binding(3)]]
var<storage, read_write> light_indices: LightIndices;
[[group(0), binding(4)]]
var<storage, read_write> light_grid: LightGrid;

// 16 * 8 * 4 = 512
var<workgroup> shared_lights: array<Light, 512>;

fn sq_dist_point_aabb(point: vec3<f32>, tile: u32) -> f32 {
	var sq_dist: f32 = 0.0;
	let current_cell = clusters.arr[tile];
	for (var i: u32 = 0u; i < 3u; i = i + 1u) {
		let v = point[i];
		if (v < current_cell.min_point[i]) {
			sq_dist = sq_dist + (current_cell.min_point[i] - v) * (current_cell.min_point[i] - v);
		}
		if (v > current_cell.max_point[i]) {
			sq_dist = sq_dist + (v - current_cell.max_point[i]) * (v - current_cell.max_point[i]);
		}
	}

	return sq_dist;
}

fn test_sphere_aabb(light: u32, tile: u32) -> bool {
	let radius = shared_lights[light].radius;
	let center = (uniforms.view * vec4<f32>(shared_lights[light].position, 1.0)).xyz;
	let squared_distance = sq_dist_point_aabb(center, tile);

	return squared_distance <= (radius * radius);
}

let NUM_CLUSTERS_X: u32 = 16u;
let NUM_CLUSTERS_Y: u32 = 8u;
let NUM_CLUSTERS_Z: u32 = 24u;

[[stage(compute), workgroup_size(16, 8, 4)]]
fn main(
	[[builtin(local_invocation_index)]] local_index: u32,
	[[builtin(global_invocation_id)]] global_id: vec3<u32>
) {
	light_indices.count = 0u;
	let thread_count = NUM_CLUSTERS_X * NUM_CLUSTERS_Y * NUM_CLUSTERS_Z;
	let light_count = arrayLength(&lights.arr);
	let num_batches = (light_count + thread_count - 1u) / thread_count;

	let tile_index =
		global_id.x +
		global_id.y * NUM_CLUSTERS_X +
		global_id.z * NUM_CLUSTERS_X * NUM_CLUSTERS_Y;

	var visible_light_count: u32 = 0u;
	var visible_light_indices: array<u32, 128>;


	for (var batch: u32 = 0u; batch < num_batches; batch = batch + 1u) {
		let light_index = batch * thread_count + local_index;

		var light = Light(vec3<f32>(0.0), vec3<f32>(0.0), 0.0, 0.0, 0u);
		if (light_index < light_count) {
			light = lights.arr[light_index];
		}
		shared_lights[local_index] = light;

		workgroupBarrier();

		for (var light: u32 = 0u; light < thread_count; light = light + 1u) {
			if (shared_lights[light].enabled == 1u) {
				if (test_sphere_aabb(light, tile_index)) {
					visible_light_indices[visible_light_count] = batch * thread_count + light;
					visible_light_count = visible_light_count + 1u;
				}
			}
		}
	}

	storageBarrier();

	let offset = atomicAdd(&light_indices.count, visible_light_count);

	for (var i: u32 = 0u; i < visible_light_count; i = i + 1u) {
		light_indices.arr[offset + i] = visible_light_indices[i];
	}

	light_grid.arr[tile_index].offset = offset;
	light_grid.arr[tile_index].count = visible_light_count;
}
