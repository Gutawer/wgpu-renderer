use core::ops::{Deref, Range};
use std::{collections::HashMap, num::NonZeroU8};

use crate::entity::*;
use crate::texture::TextureHandle;

use anyhow::*;
use itertools::*;
use ordered_float::NotNan;
use parking_lot::RwLock;
use rayon::prelude::*;
use wgpu::util::DeviceExt;

pub trait Vertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a>;
}

#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct ModelVertex {
    pub position: [NotNan<f32>; 3],
    pub tex_coord: [NotNan<f32>; 2],
    pub normal: [NotNan<f32>; 3],
    pub tangent: [NotNan<f32>; 4],
}
impl ModelVertex {
    fn from_raw(raw: RawModelVertex) -> Result<Self> {
        macro_rules! check_array {
            ($arr: expr) => {{
                let mapped = $arr.map(NotNan::new);
                for m in mapped {
                    let m = m?;
                    if m.is_infinite() {
                        bail!("infinite float in vertex");
                    }
                }
                mapped.map(Result::unwrap)
            }};
        }

        let position = check_array!(raw.position);
        let tex_coord = check_array!(raw.tex_coord);
        let normal = check_array!(raw.normal);
        let tangent = check_array!(raw.tangent);

        Ok(Self {
            position,
            tex_coord,
            normal,
            tangent,
        })
    }

    fn to_raw(self) -> RawModelVertex {
        RawModelVertex {
            position: self.position.map(NotNan::into_inner),
            tex_coord: self.tex_coord.map(NotNan::into_inner),
            normal: self.normal.map(NotNan::into_inner),
            tangent: self.tangent.map(NotNan::into_inner),
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct RawModelVertex {
    pub position: [f32; 3],
    pub tex_coord: [f32; 2],
    pub normal: [f32; 3],
    pub tangent: [f32; 4],
}

impl Vertex for RawModelVertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        const ATTRS: [wgpu::VertexAttribute; 4] = wgpu::vertex_attr_array![
            0 => Float32x3, 1 => Float32x2, 2 => Float32x3, 3 => Float32x4
        ];
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &ATTRS,
        }
    }
}

fn format(img: image::DynamicImage, srgb: bool) -> (Vec<u8>, wgpu::TextureFormat) {
    use image::DynamicImage::*;
    match (img, srgb) {
        (ImageRgba8(i), true) => (i.into_raw(), wgpu::TextureFormat::Rgba8UnormSrgb),
        (ImageRgba8(i), false) => (i.into_raw(), wgpu::TextureFormat::Rgba8Unorm),

        (ImageLuma8(i), false) => (i.into_raw(), wgpu::TextureFormat::R8Unorm),

        (ImageLumaA8(i), false) => (i.into_raw(), wgpu::TextureFormat::Rg8Unorm),

        (i @ ImageLuma8(_), true) | (i @ ImageLumaA8(_), true) | (i @ ImageRgb8(_), _) => {
            let i = i.to_rgba8();
            (
                i.into_raw(),
                if srgb {
                    wgpu::TextureFormat::Rgba8UnormSrgb
                } else {
                    wgpu::TextureFormat::Rgba8Unorm
                },
            )
        }

        _ => unimplemented!(),
    }
}

fn gltf_wrapping(w: gltf::texture::WrappingMode) -> wgpu::AddressMode {
    use gltf::texture::WrappingMode;
    use wgpu::AddressMode;

    match w {
        WrappingMode::ClampToEdge => AddressMode::ClampToEdge,
        WrappingMode::Repeat => AddressMode::Repeat,
        WrappingMode::MirroredRepeat => AddressMode::MirrorRepeat,
    }
}

fn gltf_mag_filter(f: gltf::texture::MagFilter) -> wgpu::FilterMode {
    use gltf::texture::MagFilter;
    use wgpu::FilterMode;

    match f {
        MagFilter::Nearest => FilterMode::Nearest,
        MagFilter::Linear => FilterMode::Linear,
    }
}

fn gltf_min_filter(f: gltf::texture::MinFilter) -> (wgpu::FilterMode, Option<wgpu::FilterMode>) {
    use gltf::texture::MinFilter;
    use wgpu::FilterMode;
    match f {
        MinFilter::Nearest => (FilterMode::Nearest, None),
        MinFilter::Linear => (FilterMode::Linear, None),

        MinFilter::NearestMipmapNearest => (FilterMode::Nearest, Some(FilterMode::Nearest)),
        MinFilter::LinearMipmapNearest => (FilterMode::Linear, Some(FilterMode::Nearest)),

        MinFilter::NearestMipmapLinear => (FilterMode::Nearest, Some(FilterMode::Linear)),
        MinFilter::LinearMipmapLinear => (FilterMode::Linear, Some(FilterMode::Linear)),
    }
}

struct BufferCache<'a> {
    filesystem: &'a crate::files::FileManager,
    blob: Option<&'a [u8]>,
    uri_cache: HashMap<&'a str, Vec<u8>>,
}

impl<'a> BufferCache<'a> {
    fn get_from_gltf_uri(
        &self,
        uri: &str,
        gltf_base: &url::Url,
    ) -> Result<(Option<String>, Vec<u8>)> {
        use std::io::Read;
        let fetched = self
            .filesystem
            .fetch_with_absolute(&gltf_base.join(uri).context("Failed to parse URL")?)
            .context("Failed to load URL")?;

        let file_bytes = fetched.reader.bytes().collect::<Result<Vec<_>, _>>()?;

        if let Some(m) = fetched.mediatype {
            Ok((Some(m), file_bytes))
        } else if file_bytes.starts_with(&[0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]) {
            Ok((Some("image/png".into()), file_bytes))
        } else if file_bytes.starts_with(&[0xFF, 0xD8, 0xFF]) {
            Ok((Some("image/jpeg".into()), file_bytes))
        } else if file_bytes.starts_with(&[
            0xAB, 0x4B, 0x54, 0x58, 0x20, 0x32, 0x30, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A,
        ]) {
            Ok((Some("image/ktx2".into()), file_bytes))
        } else {
            Ok((None, file_bytes))
        }
    }

    fn add(
        &mut self,
        source: gltf::buffer::Source<'a>,
        blob: Option<&'a [u8]>,
        gltf_base: &url::Url,
    ) -> Result<()> {
        match source {
            gltf::buffer::Source::Bin => {
                if blob.is_none() {
                    bail!("no blob in model");
                }
                self.blob = blob;
                return Ok(());
            }
            gltf::buffer::Source::Uri(uri) => {
                if self.uri_cache.contains_key(uri) {
                    return Ok(());
                }
            }
        }
        let uri = if let gltf::buffer::Source::Uri(u) = source {
            u
        } else {
            unreachable!()
        };
        let (_, data) = self
            .get_from_gltf_uri(uri, gltf_base)
            .context(format!("Loading from URI {:?} failed", uri))?;
        self.uri_cache.insert(uri, data);
        Ok(())
    }

    fn get(&self, source: gltf::buffer::Source) -> &[u8] {
        match source {
            gltf::buffer::Source::Bin => self.blob.unwrap(),
            gltf::buffer::Source::Uri(uri) => self.uri_cache.get(uri).unwrap().deref(),
        }
    }
}

fn buffer<'a>(buffer_cache: &'a BufferCache, buffer: gltf::Buffer) -> &'a [u8] {
    buffer_cache.get(buffer.source())
}

#[derive(thiserror::Error, Debug)]
enum ImageError {
    #[error("invalid image mime type: {0}")]
    InvalidMimeType(String),
}

#[derive(Clone, Copy)]
enum TextureCompressionType {
    BCn,
    Astc,
    Uncompressed,
}

impl TextureCompressionType {
    fn formats(self, srgb: bool) -> (libktx_rs::enums::TranscodeFormat, wgpu::TextureFormat) {
        use libktx_rs::enums::TranscodeFormat;
        use wgpu::TextureFormat;
        match self {
            TextureCompressionType::BCn if srgb => {
                (TranscodeFormat::Bc7Rgba, TextureFormat::Bc7RgbaUnormSrgb)
            }
            TextureCompressionType::BCn => (TranscodeFormat::Bc7Rgba, TextureFormat::Bc7RgbaUnorm),

            TextureCompressionType::Astc if srgb => (
                TranscodeFormat::Astc4x4Rgba,
                TextureFormat::Astc4x4RgbaUnormSrgb,
            ),
            TextureCompressionType::Astc => (
                TranscodeFormat::Astc4x4Rgba,
                TextureFormat::Astc4x4RgbaUnorm,
            ),

            TextureCompressionType::Uncompressed if srgb => {
                (TranscodeFormat::Rgba32, TextureFormat::Rgba8UnormSrgb)
            }
            TextureCompressionType::Uncompressed => {
                (TranscodeFormat::Rgba32, TextureFormat::Rgba8Unorm)
            }
        }
    }
}

struct Image {
    format: wgpu::TextureFormat,
    width: u32,
    height: u32,
    bytes: Vec<u8>,
    has_mipmaps: bool,
}

fn image_uncompressed(image: image::DynamicImage, srgb: bool) -> Image {
    use image::GenericImageView;
    let (width, height) = image.dimensions();
    let (bytes, format) = format(image, srgb);
    Image {
        format,
        bytes,
        width,
        height,
        has_mipmaps: false,
    }
}

fn image_ktx2<S: libktx_rs::RWSeekable>(
    source: S,
    srgb: bool,
    compression: TextureCompressionType,
) -> Result<Image> {
    use libktx_rs::TextureSource;
    let stream = libktx_rs::stream::RustKtxStream::new(Box::new(source))
        .map_err(|e| anyhow!(format!("KTX error code: {}", e)))?;
    let stream = std::sync::Arc::new(std::sync::Mutex::new(stream));
    let source = libktx_rs::sources::StreamSource::new(
        stream,
        libktx_rs::enums::TextureCreateFlags::LOAD_IMAGE_DATA,
    );
    let (transcode, format) = compression.formats(srgb);
    let mut texture = source.create_texture()?;
    let mut texture_ktx2 = texture
        .ktx2()
        .ok_or_else(|| anyhow!("KTX error: not ktx2"))?;
    texture_ktx2.transcode_basis(transcode, libktx_rs::enums::TranscodeFlags::empty())?;
    let (width, height) = {
        let texture_raw = texture_ktx2.handle();
        // SAFETY: the Rust `texture` this came from is still alive
        let r = unsafe { &*texture_raw };
        if r.isArray || r.isCubemap || r.baseDepth > 1 || r.numFaces != 1 {
            bail!("KTX error: unsupported texture type");
        }
        (r.baseWidth, r.baseHeight)
    };
    let mut index = texture.data_size();
    let mut bytes = vec![0; texture.data_size()];
    texture.iterate_levels(|_, _, _, _, _, pixel_data| {
        let len = pixel_data.len();
        index -= len;
        let set_region = &mut bytes[index..][..len];
        set_region.copy_from_slice(pixel_data);
        Ok(())
    })?;
    Ok(Image {
        format,
        bytes,
        width,
        height,
        has_mipmaps: true,
    })
}

fn image_from_mime_data(
    mime_type: &str,
    image_data: &[u8],
    srgb: bool,
    compression: TextureCompressionType,
) -> Result<Image> {
    match mime_type {
        "image/png" => {
            let image = image::load_from_memory_with_format(image_data, image::ImageFormat::Png)?;
            Ok(image_uncompressed(image, srgb))
        }
        "image/jpeg" => {
            let image = image::load_from_memory_with_format(image_data, image::ImageFormat::Jpeg)?;
            Ok(image_uncompressed(image, srgb))
        }
        "image/ktx2" => {
            let image_data = image_data.to_vec();
            let cursor = std::io::Cursor::new(image_data);
            image_ktx2(cursor, srgb, compression)
        }
        m => {
            bail!(ImageError::InvalidMimeType(m.to_string()));
        }
    }
}

fn image(
    buffer_cache: &BufferCache,
    image: &gltf::Image,
    srgb: bool,
    compression: TextureCompressionType,
    gltf_base: &url::Url,
) -> Result<Image> {
    match image.source() {
        gltf::image::Source::View { view, mime_type } => {
            let buffer = buffer(buffer_cache, view.buffer());
            let image_data = &buffer[view.offset()..][..view.length()];
            image_from_mime_data(mime_type, image_data, srgb, compression)
        }
        gltf::image::Source::Uri {
            uri,
            mime_type: mime_type_from_image,
        } => {
            let (mime_type_from_data, image_data) =
                buffer_cache.get_from_gltf_uri(uri, gltf_base)?;
            let mime_type = if let Some(m) = mime_type_from_image {
                m
            } else if mime_type_from_data.is_some() {
                mime_type_from_data.as_ref().unwrap()
            } else {
                bail!("no MIME type given and the data didn't look like a supported image");
            };
            image_from_mime_data(mime_type, &image_data, srgb, compression)
        }
    }
}

fn gltf_texture(
    name: &str,
    (width, height, pixels): (u32, u32, &[u8]),
    has_mipmaps: bool,
    format: wgpu::TextureFormat,
    sampler: gltf::texture::Sampler,
    device_queue: crate::DeviceQueue,
    tex_man: &crate::texture::TextureManager,
) -> TextureHandle {
    let (min_filter, mipmap_filter) = gltf_min_filter(
        sampler
            .min_filter()
            .unwrap_or(gltf::texture::MinFilter::LinearMipmapLinear),
    );
    let mag_filter = gltf_mag_filter(
        sampler
            .mag_filter()
            .unwrap_or(gltf::texture::MagFilter::Linear),
    );
    let address_mode_u = gltf_wrapping(sampler.wrap_s());
    let address_mode_v = gltf_wrapping(sampler.wrap_t());

    let (pixels, mipmap_filter) = if let Some(m) = mipmap_filter {
        (
            if has_mipmaps {
                crate::texture::Pixels::WithMipmaps(pixels)
            } else {
                crate::texture::Pixels::NeedsMipmaps(pixels)
            },
            m,
        )
    } else {
        (
            if has_mipmaps {
                crate::texture::Pixels::WithMipmaps(pixels)
            } else {
                crate::texture::Pixels::NoMipmaps(pixels)
            },
            wgpu::FilterMode::Linear,
        )
    };

    let sampler = device_queue
        .device
        .create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u,
            address_mode_v,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter,
            min_filter,
            mipmap_filter,
            anisotropy_clamp: Some(NonZeroU8::new(16).unwrap()),
            ..Default::default()
        });

    tex_man.add_from_raw_pixel_bytes(device_queue, pixels, (width, height), name, format, sampler)
}

#[derive(Debug)]
pub struct MeshManager {
    meshes: RwLock<MeshList>,
}
crate::manager::manager!(MeshManager, MeshList, meshes, MeshHandle, Mesh);

impl MeshManager {
    pub fn new() -> Self {
        Self {
            meshes: RwLock::new(MeshList(vec![])),
        }
    }
}

#[derive(Debug)]
pub enum ModelNodeKind {
    Mesh(MeshHandle),
    Unsupported,
}

#[derive(Debug)]
pub struct ModelNode {
    pub children: Vec<usize>,
    pub transform: TransformData,
    pub kind: ModelNodeKind,
}

#[derive(Debug)]
pub struct Model {
    pub meshes: Vec<MeshHandle>,
    pub materials: Vec<MaterialHandle>,
    pub nodes: Vec<ModelNode>,
    pub root_model_node_indices: Vec<usize>,
}

impl Model {
    pub fn instantiate_parented_to(&self, entity_manager: &EntityManager, parent: EntityHandle) {
        fn recurse(
            s: &Model,
            entity_manager: &EntityManager,
            parent: EntityHandle,
            child: &ModelNode,
        ) {
            let mesh = if let ModelNodeKind::Mesh(m) = child.kind {
                Some(m)
            } else {
                None
            };
            let child_entity = Entity::new(mesh, child.transform);
            let child_entity = entity_manager.add(child_entity);
            entity_manager.make_parent(parent, child_entity);
            for &c in child.children.iter() {
                let child_child = &s.nodes[c];
                recurse(s, entity_manager, child_entity, child_child);
            }
        }

        let roots = self.root_model_node_indices.iter().map(|i| &self.nodes[*i]);
        for r in roots {
            recurse(self, entity_manager, parent, r);
        }
    }

    pub fn load(
        device_queue: crate::DeviceQueue,
        layout: &wgpu::BindGroupLayout,
        filesystem: &crate::files::FileManager,
        resource_managers: &crate::resource_managers::ResourceManagers,
        archive: crate::files::ArchiveHandle,
        path: &str,
    ) -> Result<Self> {
        let features = device_queue.device.features();
        let compression = if features.contains(wgpu::Features::TEXTURE_COMPRESSION_BC) {
            TextureCompressionType::BCn
        } else if features.contains(wgpu::Features::TEXTURE_COMPRESSION_ASTC_LDR) {
            TextureCompressionType::Astc
        } else {
            TextureCompressionType::Uncompressed
        };

        let gltf_base =
            crate::files::FileManager::resolve(archive, path).context("Failed to parse path")?;
        let file = filesystem
            .fetch_with_absolute(&gltf_base)
            .context(format!("Failed to load model main file {:?}", path))?
            .reader;
        let reader = std::io::BufReader::new(file);
        let gltf_model = gltf::Gltf::from_reader(reader)?;
        let document = gltf_model.document;
        let mut buffer_cache = BufferCache {
            filesystem,
            blob: None,
            uri_cache: HashMap::new(),
        };
        for b in document.buffers() {
            buffer_cache
                .add(b.source(), gltf_model.blob.as_deref(), &gltf_base)
                .context("Loading buffer to cache failed")?;
        }
        let buffer_cache_ref = &buffer_cache;

        let materials = document
            .materials()
            .map(|mat| {
                let mut count = 0;
                let mut get_tex = |tex: &gltf::Texture, srgb: bool| -> Result<TextureHandle> {
                    let (img_fallback, img_ktx) = tex.basisu_sources();
                    let img = if let Some(i) = img_ktx {
                        i
                    } else {
                        img_fallback.unwrap()
                    };
                    let sampler = tex.sampler();
                    let image = image(buffer_cache_ref, &img, srgb, compression, &gltf_base)
                        .context("Image loading failed")?;
                    let name =
                        &format!("{:?}-{}", path, img.name().unwrap_or(&format!("{}", count)));
                    count += 1;
                    Ok(gltf_texture(
                        name,
                        (image.width, image.height, &image.bytes),
                        image.has_mipmaps,
                        image.format,
                        sampler,
                        device_queue,
                        &resource_managers.textures,
                    ))
                };

                let pbr_metallic_roughness = mat.pbr_metallic_roughness();
                let albedo = pbr_metallic_roughness
                    .base_color_texture()
                    .context("Albedo texture missing")?
                    .texture();
                let albedo_texture =
                    get_tex(&albedo, true).context("Albedo texture loading failed")?;

                let normal = mat
                    .normal_texture()
                    .context("Missing normal texture")?
                    .texture();
                let normal_texture =
                    get_tex(&normal, false).context("Normal texture loading failed")?;

                let metallic_roughness = pbr_metallic_roughness
                    .metallic_roughness_texture()
                    .context("Missing metallic/roughness texture")?
                    .texture();
                let metallic_roughness_texture = get_tex(&metallic_roughness, false)
                    .context("Metallic/Roughness texture loading failed")?;

                let ao = mat.occlusion_texture().map(|t| t.texture());
                let ao_texture = ao
                    .as_ref()
                    .map(|i| {
                        if i.index() == metallic_roughness.index() {
                            Ok(metallic_roughness_texture)
                        } else {
                            get_tex(i, false)
                        }
                    })
                    .unwrap_or_else(|| {
                        Ok(resource_managers
                            .textures
                            .add_from_path(
                                device_queue,
                                "res/white.png",
                                false,
                                crate::texture::Texture::default_sampler(device_queue),
                                false,
                            )
                            .unwrap())
                    })
                    .context("AO texture loading failed")?;

                let emissive = mat.emissive_texture().map(|t| t.texture());
                let emissive_texture = emissive
                    .as_ref()
                    .map(|i| get_tex(i, true))
                    .unwrap_or_else(|| {
                        Ok(resource_managers
                            .textures
                            .add_from_path(
                                device_queue,
                                "res/white.png",
                                true,
                                crate::texture::Texture::default_sampler(device_queue),
                                false,
                            )
                            .unwrap())
                    })
                    .context("Emissive texture loading failed")?;

                let emissive_factor = mat.emissive_factor();

                Ok(resource_managers.materials.add(Material::new(
                    device_queue,
                    mat.name().unwrap_or("No Name"),
                    Textures {
                        albedo: albedo_texture,
                        normal: normal_texture,
                        metallic_roughness: metallic_roughness_texture,
                        ao: ao_texture,
                        emissive: emissive_texture,
                    },
                    emissive_factor,
                    &resource_managers.textures,
                    layout,
                )))
            })
            .collect::<Result<Vec<_>>>()
            .context("Material loading failed")?;

        let meshes = document
            .meshes()
            .par_bridge()
            .map(|m| {
                let name = m.name().unwrap_or("No Name");

                let primitives = m
                    .primitives()
                    .par_bridge()
                    .map(|primitive| {
                        let get_buffer = move |b: gltf::Buffer| -> Option<&[u8]> {
                            let slice = buffer(buffer_cache_ref, b);
                            Some(slice)
                        };
                        let reader = primitive.reader(get_buffer);

                        let get_indices = |count: u32| {
                            if let Some(indices) = reader.read_indices() {
                                indices.into_u32().into_iter().collect_vec()
                            } else {
                                (0..count).collect_vec()
                            }
                        };

                        let positions = reader
                            .read_positions()
                            .context("Model had positions missing")?;
                        let tex_coords = reader
                            .read_tex_coords(0)
                            .context("Model had tex_coords 0 missing")?
                            .into_f32();
                        let normals = reader.read_normals().context("Model had normals missing")?;
                        let tangents = reader.read_tangents();

                        let (vertices, indices) = if let Some(tangents) = tangents {
                            let iter = izip!(positions, tex_coords, normals, tangents);
                            let vertices = iter
                                .map(|(position, tex_coord, normal, tangent)| {
                                    ModelVertex::from_raw(RawModelVertex {
                                        position: [position[0], -position[2], position[1]],
                                        tex_coord,
                                        normal: [normal[0], -normal[2], normal[1]],
                                        tangent: [tangent[0], -tangent[2], tangent[1], tangent[3]],
                                    })
                                })
                                .collect::<Result<Vec<_>>>()
                                .context("Model contained invalid float(s)")?;
                            let indices = get_indices(vertices.len() as u32);
                            (vertices, indices)
                        } else {
                            let iter = izip!(positions, tex_coords, normals);
                            let vertices = iter
                                .map(|(position, tex_coord, normal)| {
                                    ModelVertex::from_raw(RawModelVertex {
                                        position: [position[0], -position[2], position[1]],
                                        tex_coord,
                                        normal: [normal[0], -normal[2], normal[1]],
                                        tangent: [0.0; 4],
                                    })
                                })
                                .collect::<Result<Vec<_>>>()
                                .context("Model contained invalid float(s)")?;
                            let indices = get_indices(vertices.len() as u32);
                            let mut unwelded = UnweldedPrimitive::from_welded(&vertices, &indices);
                            mikktspace::generate_tangents(&mut unwelded);
                            let (vertices, indices) = unwelded.to_welded();
                            (vertices, indices)
                        };

                        let vertices = vertices.iter().map(|v| v.to_raw()).collect_vec();

                        let vertex_buffer = device_queue.device.create_buffer_init(
                            &wgpu::util::BufferInitDescriptor {
                                label: Some(&format!("{:?} Vertex Buffer", name)),
                                contents: bytemuck::cast_slice(&vertices),
                                usage: wgpu::BufferUsages::VERTEX,
                            },
                        );
                        let index_buffer = device_queue.device.create_buffer_init(
                            &wgpu::util::BufferInitDescriptor {
                                label: Some(&format!("{:?} Index Buffer", name)),
                                contents: bytemuck::cast_slice(&indices),
                                usage: wgpu::BufferUsages::INDEX,
                            },
                        );

                        Ok(Primitive {
                            index_count: indices.len() as u32,
                            vertex_buffer,
                            index_buffer,
                            material: materials[primitive.material().index().unwrap()],
                        })
                    })
                    .collect::<Result<Vec<_>>>()
                    .context("Primitive loading failed")?;

                Ok(resource_managers.meshes.add(Mesh {
                    name: name.to_string(),
                    primitives,
                    instances: None,
                }))
            })
            .collect::<Result<Vec<_>>>()
            .context("Mesh loading failed")?;

        let nodes = document
            .nodes()
            .map(|n| {
                let kind = if let Some(m) = n.mesh() {
                    ModelNodeKind::Mesh(meshes[m.index()])
                } else {
                    ModelNodeKind::Unsupported
                };

                let (position, rotation, scale) = n.transform().decomposed();
                let position = cgmath::Vector3::new(position[0], -position[2], position[1]);
                let rotation =
                    cgmath::Quaternion::new(rotation[3], rotation[0], -rotation[2], rotation[1]);
                let scale = cgmath::Vector3::new(scale[0], scale[2], scale[1]);
                ModelNode {
                    children: n.children().map(|c| c.index()).collect::<Vec<_>>(),
                    transform: crate::entity::TransformData {
                        position,
                        rotation,
                        scale,
                    },
                    kind,
                }
            })
            .collect_vec();

        let root_model_node_indices = document
            .scenes()
            .next()
            .unwrap()
            .nodes()
            .map(|n| n.index())
            .collect_vec();

        Ok(Self {
            meshes,
            materials,
            nodes,
            root_model_node_indices,
        })
    }
}

#[derive(Debug)]
pub(crate) struct MeshInstances {
    pub buffer: wgpu::Buffer,
    pub num_instances: u32,
}

#[derive(Debug)]
pub struct Mesh {
    pub name: String,
    pub primitives: Vec<Primitive>,
    pub(crate) instances: Option<MeshInstances>,
}

#[derive(Debug)]
pub struct Primitive {
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    index_count: u32,
    material: MaterialHandle,
}

#[derive(Debug)]
pub struct MaterialManager {
    materials: RwLock<MaterialList>,
}
crate::manager::manager!(
    MaterialManager,
    MaterialList,
    materials,
    MaterialHandle,
    Material
);

impl MaterialManager {
    pub fn new() -> Self {
        Self {
            materials: RwLock::new(MaterialList(vec![])),
        }
    }
}

pub struct Textures {
    albedo: TextureHandle,
    normal: TextureHandle,
    metallic_roughness: TextureHandle,
    ao: TextureHandle,
    emissive: TextureHandle,
}

#[derive(Debug)]
pub struct Material {
    pub name: String,
    pub bind_group: wgpu::BindGroup,
}

impl Material {
    pub fn get_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler {
                        comparison: false,
                        filtering: true,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 3,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler {
                        comparison: false,
                        filtering: true,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 4,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 5,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler {
                        comparison: false,
                        filtering: true,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 6,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 7,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler {
                        comparison: false,
                        filtering: true,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 8,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 9,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler {
                        comparison: false,
                        filtering: true,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 10,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
            ],
            label: Some("texture_bind_group_layout"),
        })
    }

    pub fn new(
        device_queue: crate::DeviceQueue,
        name: &str,
        textures: Textures,
        emissive_factor: [f32; 3],
        tex_man: &crate::texture::TextureManager,
        layout: &wgpu::BindGroupLayout,
    ) -> Self {
        let emissive_factor_buffer =
            device_queue
                .device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("{} emissive factor buffer", name)),
                    contents: bytemuck::cast_slice(&emissive_factor),
                    usage: wgpu::BufferUsages::UNIFORM,
                });
        let bind_group = device_queue
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::TextureView(
                            &tex_man.get(textures.albedo).view,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::Sampler(
                            &tex_man.get(textures.albedo).sampler,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::TextureView(
                            &tex_man.get(textures.normal).view,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 3,
                        resource: wgpu::BindingResource::Sampler(
                            &tex_man.get(textures.normal).sampler,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 4,
                        resource: wgpu::BindingResource::TextureView(
                            &tex_man.get(textures.metallic_roughness).view,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 5,
                        resource: wgpu::BindingResource::Sampler(
                            &tex_man.get(textures.metallic_roughness).sampler,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 6,
                        resource: wgpu::BindingResource::TextureView(
                            &tex_man.get(textures.ao).view,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 7,
                        resource: wgpu::BindingResource::Sampler(&tex_man.get(textures.ao).sampler),
                    },
                    wgpu::BindGroupEntry {
                        binding: 8,
                        resource: wgpu::BindingResource::TextureView(
                            &tex_man.get(textures.emissive).view,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 9,
                        resource: wgpu::BindingResource::Sampler(
                            &tex_man.get(textures.emissive).sampler,
                        ),
                    },
                    wgpu::BindGroupEntry {
                        binding: 10,
                        resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                            buffer: &emissive_factor_buffer,
                            offset: 0,
                            size: None,
                        }),
                    },
                ],
                label: Some(name),
            });

        Self {
            name: String::from(name),
            bind_group,
        }
    }
}

struct UnweldedPrimitive(Vec<[ModelVertex; 3]>);

impl UnweldedPrimitive {
    fn from_welded(vertices: &[ModelVertex], indices: &[u32]) -> Self {
        let unwelded_verts = indices
            .chunks_exact(3)
            .map(|is| {
                [
                    vertices[is[0] as usize],
                    vertices[is[1] as usize],
                    vertices[is[2] as usize],
                ]
            })
            .collect_vec();
        Self(unwelded_verts)
    }

    fn to_welded(&self) -> (Vec<ModelVertex>, Vec<u32>) {
        let mut output_vertices = vec![];
        let mut output_indices = vec![];
        let mut vert_to_index_map = HashMap::new();
        for vs in self.0.iter() {
            for v in vs.iter() {
                output_indices.push(*vert_to_index_map.entry(v).or_insert_with(|| {
                    output_vertices.push(*v);
                    (output_vertices.len() - 1) as u32
                }));
            }
        }
        (output_vertices, output_indices)
    }
}

impl mikktspace::Geometry for UnweldedPrimitive {
    fn num_faces(&self) -> usize {
        self.0.len()
    }
    fn num_vertices_of_face(&self, _: usize) -> usize {
        3
    }
    fn position(&self, face: usize, vert: usize) -> [f32; 3] {
        self.0[face][vert].position.map(NotNan::into_inner)
    }
    fn normal(&self, face: usize, vert: usize) -> [f32; 3] {
        self.0[face][vert].normal.map(NotNan::into_inner)
    }
    fn tex_coord(&self, face: usize, vert: usize) -> [f32; 2] {
        self.0[face][vert].tex_coord.map(NotNan::into_inner)
    }
    fn set_tangent_encoded(&mut self, tangent: [f32; 4], face: usize, vert: usize) {
        self.0[face][vert].tangent = tangent.map(|t| NotNan::new(t).unwrap());
        self.0[face][vert].tangent[3] *= -1.0;
    }
}

pub trait DrawModel<'a, 'b>
where
    'b: 'a,
{
    fn draw_mesh(
        &mut self,
        mesh: &'b Mesh,
        materials: &'a MaterialList,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        env: &'b wgpu::BindGroup,
    );
    fn draw_mesh_instanced(
        &mut self,
        mesh: &'b Mesh,
        materials: &'a MaterialList,
        instances: Range<u32>,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        env: &'b wgpu::BindGroup,
    );
}

impl<'a, 'b, T: wgpu::util::RenderEncoder<'a>> DrawModel<'a, 'b> for T
where
    'b: 'a,
{
    fn draw_mesh(
        &mut self,
        mesh: &'b Mesh,
        materials: &'a MaterialList,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        env: &'b wgpu::BindGroup,
    ) {
        self.draw_mesh_instanced(mesh, materials, 0..1, uniforms, cluster, env);
    }
    fn draw_mesh_instanced(
        &mut self,
        mesh: &'b Mesh,
        materials: &'a MaterialList,
        instances: Range<u32>,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        env: &'b wgpu::BindGroup,
    ) {
        for p in mesh.primitives.iter() {
            let material = materials.get(p.material);
            self.set_vertex_buffer(0, p.vertex_buffer.slice(..));
            self.set_index_buffer(p.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
            self.set_bind_group(0, &material.bind_group, &[]);
            self.set_bind_group(1, uniforms, &[]);
            self.set_bind_group(2, cluster, &[]);
            self.set_bind_group(3, env, &[]);
            self.draw_indexed(0..p.index_count, 0, instances.clone());
        }
    }
}

pub trait DrawModelRaw<'a, 'b>
where
    'b: 'a,
{
    fn draw_mesh_raw(&mut self, mesh: &'b Mesh);
    fn draw_mesh_instanced_raw(&mut self, mesh: &'b Mesh, instances: Range<u32>);
}

impl<'a, 'b, T: wgpu::util::RenderEncoder<'a>> DrawModelRaw<'a, 'b> for T
where
    'b: 'a,
{
    fn draw_mesh_raw(&mut self, mesh: &'b Mesh) {
        self.draw_mesh_instanced_raw(mesh, 0..1);
    }
    fn draw_mesh_instanced_raw(&mut self, mesh: &'b Mesh, instances: Range<u32>) {
        for p in mesh.primitives.iter() {
            self.set_vertex_buffer(0, p.vertex_buffer.slice(..));
            self.set_index_buffer(p.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
            self.draw_indexed(0..p.index_count, 0, instances.clone());
        }
    }
}

pub trait DrawLight<'a, 'b>
where
    'b: 'a,
{
    fn draw_light_meshes(
        &mut self,
        mesh: &'b Mesh,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        num_lights: u32,
    );
}

impl<'a, 'b, T: wgpu::util::RenderEncoder<'a>> DrawLight<'a, 'b> for T
where
    'b: 'a,
{
    fn draw_light_meshes(
        &mut self,
        mesh: &'b Mesh,
        uniforms: &'b wgpu::BindGroup,
        cluster: &'b wgpu::BindGroup,
        num_lights: u32,
    ) {
        let p = &mesh.primitives[0];
        self.set_vertex_buffer(0, p.vertex_buffer.slice(..));
        self.set_index_buffer(p.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
        self.set_bind_group(0, uniforms, &[]);
        self.set_bind_group(1, cluster, &[]);
        self.draw_indexed(0..p.index_count, 0, 0..num_lights);
    }
}
