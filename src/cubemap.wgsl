[[block]]
struct ViewProj {
	view_proj: mat4x4<f32>;
};

[[group(0), binding(0)]]
var<uniform> view_proj: ViewProj;
struct VertexInput {
	[[builtin(instance_index)]] instance: u32;
	[[location(0)]] position: vec3<f32>;
};

struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] local_pos: vec3<f32>;
	[[location(1), interpolate(flat)]] instance: u32;
};

[[stage(vertex)]]
fn main(
	model: VertexInput
) -> VertexOutput {
	var out: VertexOutput;
	out.local_pos = model.position;
	out.clip_position = view_proj.view_proj * vec4<f32>(out.local_pos, 1.0);
	out.clip_position.y = -out.clip_position.y;
	out.instance = model.instance;
	return out;
}

[[group(0), binding(1)]]
var t_equi: texture_2d<f32>;
[[group(0), binding(2)]]
var s_equi: sampler;

let inv_atan: vec2<f32> = vec2<f32>(0.1591, 0.3183);
fn sample_spherical_map(v: vec3<f32>) -> vec2<f32> {
	let uv = vec2<f32>(atan2(-v.x, -v.y), asin(v.z));
	let uv = uv * inv_atan;
	let uv = uv + 0.5;
	return vec2<f32>(uv.x, 1.0 - uv.y);
}

[[stage(fragment)]]
fn equi_sample(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	let uv = sample_spherical_map(normalize(in.local_pos));
	let color = textureSample(t_equi, s_equi, uv).rgb;
	return vec4<f32>(color, 1.0);
}

[[group(0), binding(1)]]
var t_env: texture_cube<f32>;
[[group(0), binding(2)]]
var s_env: sampler;

let PI: f32 = 3.14159265359;

[[stage(fragment)]]
fn diffuse_convolution(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	let normal = normalize(in.local_pos);
	var irradiance: vec3<f32> = vec3<f32>(0.0);

	let up = vec3<f32>(0.0, 0.0, 1.0);
	let right = normalize(cross(up, normal));
	let up = normalize(cross(normal, right));

	let sample_delta = 0.025;
	var nr_samples: u32 = 0u;
	for (var phi: f32 = 0.0; phi < 2.0 * PI; phi = phi + sample_delta) {
		for (var theta: f32 = 0.0; theta < 0.5 * PI; theta = theta + sample_delta) {
			let tangent_sample = vec3<f32>(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
			let sample_vec = tangent_sample.x * right + tangent_sample.y * up + tangent_sample.z * normal;

			irradiance = irradiance + textureSample(t_env, s_env, sample_vec).rgb * cos(theta) * sin(theta);
			nr_samples = nr_samples + 1u;
		}
	}
	let irradiance = PI * irradiance * (1.0 / f32(nr_samples));

	return vec4<f32>(irradiance, 1.0);
}

// TODO: replace these constants with hex ones once naga supports it
fn radical_inverse_vdc(bits: u32) -> f32 {
    let bits = (bits << 16u) | (bits >> 16u);
	//                  0x55555555                     0xAAAAAAAA
    let bits = ((bits & 1431655765u) << 1u) | ((bits & 2863311530u) >> 1u);
	//                  0x33333333                     0xCCCCCCCC
    let bits = ((bits &  858993459u) << 2u) | ((bits & 3435973836u) >> 2u);
	//                  0x0F0F0F0F                     0xF0F0F0F0
    let bits = ((bits &  252645135u) << 4u) | ((bits & 4042322160u) >> 4u);
	//                  0x00FF00FF                     0xFF00FF00
    let bits = ((bits &   16711935u) << 8u) | ((bits & 4278255360u) >> 8u);
	return f32(bits) * 2.3283064365386963e-10;
}
fn hammersley(i: u32, n: u32) -> vec2<f32> {
	return vec2<f32>(f32(i) / f32(n), radical_inverse_vdc(i));
}

fn importance_sample_ggx(x_i: vec2<f32>, n: vec3<f32>, roughness: f32) -> vec3<f32> {
	let a = roughness * roughness;

	let phi = 2.0 * PI * x_i.x;
	let cos_theta = sqrt((1.0 - x_i.y) / (1.0 + (a*a - 1.0) * x_i.y));
	let sin_theta = sqrt(1.0 - cos_theta * cos_theta);

	let h = vec3<f32>(
		cos(phi) * sin_theta,
		sin(phi) * sin_theta,
		cos_theta
	);

	var up: vec3<f32>;
	if (abs(n.z) < 0.999) { up = vec3<f32>(0.0, 0.0, 1.0); }
	else { up = vec3<f32>(1.0, 0.0, 0.0); }
	let up = up;
	let tangent = normalize(cross(up, n));
	let bitangent = cross(n, tangent);

	let sample_vec = tangent * h.x + bitangent * h.y + n * h.z;
	return normalize(sample_vec);
}

[[stage(fragment)]]
fn specular_convolution(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	let SPECULAR_MIPS = 5.0;
	let roughness = f32(in.instance) / (SPECULAR_MIPS - 1.0);

	let n = normalize(in.local_pos);
	let r = n;
	let v = r;

	let SAMPLE_COUNT = 1024u;
	var total_weight: f32 = 0.0;
	var prefiltered_color = vec3<f32>(0.0);
	for (var i: u32 = 0u; i < SAMPLE_COUNT; i = i + 1u) {
		let x_i = hammersley(i, SAMPLE_COUNT);
		let h = importance_sample_ggx(x_i, n, roughness);
		let l = normalize(2.0 * dot(v, h) * h - v);

		let n_dot_l = max(dot(n, l), 0.0);
		let sample = textureSample(t_env, s_env, l).rgb;
		if (n_dot_l > 0.0) {
			prefiltered_color = prefiltered_color + sample * n_dot_l;
			total_weight = total_weight + n_dot_l;
		}
	}
	let prefiltered_color = prefiltered_color / total_weight;

	return vec4<f32>(prefiltered_color, 1.0);
}

struct BRDFVertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] tex_coord: vec2<f32>;
};

fn geometry_schlick_ggx(n_dot_v: f32, roughness: f32) -> f32 {
	let a = roughness;
	let k = (a * a) / 2.0;

	let nom = n_dot_v;
	let denom = n_dot_v * (1.0 - k) + k;

	return nom / denom;
}
fn geometry_smith(n: vec3<f32>, v: vec3<f32>, l: vec3<f32>, roughness: f32) -> f32 {
	let n_dot_v = max(dot(n, v), 0.0);
	let n_dot_l = max(dot(n, l), 0.0);
	let ggx2 = geometry_schlick_ggx(n_dot_v, roughness);
	let ggx1 = geometry_schlick_ggx(n_dot_l, roughness);

	return ggx1 * ggx2;
}
fn integrate_brdf(n_dot_v: f32, roughness: f32) -> vec2<f32> {
	let v = vec3<f32>(
		sqrt(1.0 - n_dot_v * n_dot_v),
		0.0,
		n_dot_v
	);

	var a: f32 = 0.0;
	var b: f32 = 0.0;

	let n = vec3<f32>(0.0, 0.0, 1.0);

	let SAMPLE_COUNT = 1024u;
	for (var i: u32 = 0u; i < SAMPLE_COUNT; i = i + 1u) {
		let x_i = hammersley(i, SAMPLE_COUNT);
		let h = importance_sample_ggx(x_i, n, roughness);
		let l = normalize(2.0 * dot(v, h) * h - v);

		let n_dot_l = max(l.z, 0.0);
		let n_dot_h = max(h.z, 0.0);
		let v_dot_h = max(dot(v, h), 0.0);

		if (n_dot_l > 0.0) {
			let g = geometry_smith(n, v, l, roughness);
			let g_vis = (g * v_dot_h) / (n_dot_h * n_dot_v);
			let f_c = pow(1.0 - v_dot_h, 5.0);

			a = a + (1.0 - f_c) * g_vis;
			b = b + f_c * g_vis;
		}
	}
	let a = a / f32(SAMPLE_COUNT);
	let b = b / f32(SAMPLE_COUNT);
	return vec2<f32>(a, b);
}

[[stage(fragment)]]
fn integrate_brdf_main(in: BRDFVertexOutput) -> [[location(0)]] vec2<f32> {
	let integrated_brdf = integrate_brdf(in.tex_coord.x, in.tex_coord.y);
	return integrated_brdf;
}
