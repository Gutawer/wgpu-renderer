#[derive(Debug)]
pub struct ResourceManagers {
    pub textures: crate::texture::TextureManager,
    pub meshes: crate::model::MeshManager,
    pub materials: crate::model::MaterialManager,
}
