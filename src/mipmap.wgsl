struct VertexOutput {
	[[builtin(position)]] clip_position: vec4<f32>;
	[[location(0)]] tex_coord: vec2<f32>;
};

[[group(0), binding(0)]]
var t: texture_2d<f32>;
[[group(0), binding(1)]]
var s: sampler;

[[stage(fragment)]]
fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
	return textureSample(t, s, in.tex_coord);
}
