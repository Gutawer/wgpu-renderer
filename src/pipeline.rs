pub struct RenderPipelineConfig<'a> {
    pub label: &'a str,
    pub device_queue: crate::DeviceQueue<'a>,
    pub layout: &'a wgpu::PipelineLayout,
    pub color_formats: &'a [wgpu::TextureFormat],
    pub depth: Option<(wgpu::TextureFormat, wgpu::CompareFunction)>,
    pub cull_mode: Option<wgpu::Face>,
    pub vertex_layouts: &'a [wgpu::VertexBufferLayout<'a>],
    pub vert: (&'a wgpu::ShaderModule, &'a str),
    pub frag: (&'a wgpu::ShaderModule, &'a str),
}

pub fn create_render_pipeline(
    RenderPipelineConfig {
        label,
        device_queue,
        layout,
        color_formats,
        depth,
        cull_mode,
        vertex_layouts,
        vert: (module_vert, entry_vert),
        frag: (module_frag, entry_frag),
    }: RenderPipelineConfig,
) -> wgpu::RenderPipeline {
    let targets = color_formats
        .iter()
        .map(|color_format| wgpu::ColorTargetState {
            format: *color_format,
            blend: Some(wgpu::BlendState {
                alpha: wgpu::BlendComponent::REPLACE,
                color: wgpu::BlendComponent::REPLACE,
            }),
            write_mask: wgpu::ColorWrites::ALL,
        })
        .collect::<Vec<_>>();

    device_queue
        .device
        .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some(label),
            layout: Some(layout),
            vertex: wgpu::VertexState {
                module: module_vert,
                entry_point: entry_vert,
                buffers: vertex_layouts,
            },
            fragment: Some(wgpu::FragmentState {
                module: module_frag,
                entry_point: entry_frag,
                targets: &targets,
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode,
                polygon_mode: wgpu::PolygonMode::Fill,
                clamp_depth: false,
                conservative: false,
            },
            depth_stencil: depth.map(|(depth_format, depth_func)| wgpu::DepthStencilState {
                format: depth_format,
                depth_write_enabled: true,
                depth_compare: depth_func,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
        })
}
